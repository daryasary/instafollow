from rest_framework.authentication import get_authorization_header
from rest_framework.permissions import BasePermission
from django.conf import settings


class IsFromReportingPanel(BasePermission):
    """
    Allow access if request is come from reporting panel
    """

    keyword = 'Token'

    def has_permission(self, request, view):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return False

        if len(auth) == 1 or len(auth) > 2:
            return False

        token = auth[1].decode("utf-8")

        if token == settings.REPORTING_TOKEN:
            return True

        return False
