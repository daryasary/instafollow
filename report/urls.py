from django.conf.urls import url
from .views import fast_charge_exchange_value


urlpatterns = [
    url(r'^fast_charge_exchange_value/$', fast_charge_exchange_value, name='fast_charge_exchange_value'),
]