from datetime import datetime
from django.db import connection
from django.db.models import Sum, Count
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from rest_framework.response import Response
from rest_framework.views import APIView

from account.models import User
from intermediator.models import ExchangeValue
from report.permissions import IsFromReportingPanel
from report.decorators import cached_property
from report.report_patterns import generate_report_pattern, table_pattern, \
    filter_pattern, chart_pattern


class FCExchangeValue(APIView):
    permission_classes = (IsFromReportingPanel,)

    @cached_property
    def exchanges(self):
        if self.request.method == 'POST':
            ex = ExchangeValue.objects.select_related(
                'sender', 'through'
            ).filter(**self.check_date_filter[0])
        else:
            ex = ExchangeValue.objects.select_related('sender', 'through')
        return ex

    @cached_property
    def check_date_filter(self):
        start_date_str = self.request.data['start_date']
        start_date = datetime.strptime(start_date_str, '%Y-%m-%d')

        end_date_str = self.request.data['end_date']
        end_date = datetime.strptime(end_date_str, '%Y-%m-%d')

        return (
            {'created_time__gte': start_date, 'created_time__lte': end_date},
            end_date > start_date
        )

    @property
    def date_filter(self):
        return filter_pattern('date_filter', 'date_georgia', [])

    def generate_data(self):
        return [
            [
                ex.sender.user.username,
                ex.through.name,
                ex.receiver,
                ex.value,
                ex.created_time.strftime('%y-%m-%d %H:%M')
            ] for ex in self.exchanges[:100]
            ]

    def get_trunc_date(self, unit='month', filters=False):
        truncate_date = connection.ops.date_trunc_sql(unit, 'created_time')
        ex = self.exchanges.extra(
            {unit: truncate_date}
        ).values(
            unit
        ).annotate(
            count=Count('pk'),
            sum=Sum('value')
        ).order_by(unit)

        if filters:
            if unit == 'month':
                return [i[unit].strftime("%B") for i in ex]
            elif unit == 'day':
                return [i[unit].strftime("%b/%d") for i in ex]
        else:
            return ex

    def get_summary(self):
        total = self.exchanges.aggregate(all=Sum('value'))
        return [{"text": _('Total Exchanges:'), "value": total['all']},]

    def distinct_instafollow_users(self):
        users = User.objects.filter(exchanges__isnull=False).distinct()
        return [
            [
                user.username,
                ', '.join(user.exchanges.values_list(
                    'receiver',
                    flat=True
                ).distinct())
            ] for user in users
            ]

    def distinct_fastcharge_users(self):
        exchanges = self.exchanges.select_related(
            'sender', 'through'
        ).values_list(
            'sender__username', 'receiver'
        )

        tmp = dict()
        for o in exchanges:
            if o[1] in tmp.keys():
                tmp[o[1]].add(o[0])
            else:
                tmp[o[1]] = set([o[0]])
        return tmp.items()

    def generate_month_bar_chart(self):
        data = self.get_trunc_date(unit='month')
        month_bar_chart = chart_pattern(_('Month rate'), 'chart-column-bar')
        month_bar_chart["graph_data"]['x_title'] = 'Month'
        month_bar_chart["graph_data"]['y_title'] = 'Count'

        for i in data:
            month_bar_chart["graph_data"]["data"].append(
                {"value": i['count'], "category": i['month'].strftime('%B'),
                 "name": _("Total exchanges")}
            )
            month_bar_chart["graph_data"]["data"].append(
                {"value": i['sum'], "category": i['month'].strftime('%B'),
                 "name": _("Total values")}
            )

        return month_bar_chart

    def generate_day_bar_chart(self):
        data = self.get_trunc_date(unit='day').reverse()[:30]
        day_bar_chart = chart_pattern(_('Days rate'), 'chart-column-bar')
        day_bar_chart["graph_data"]['x_title'] = 'Day'
        day_bar_chart["graph_data"]['y_title'] = 'Count'

        for i in data:
            day_bar_chart["graph_data"]["data"].append(
                {"value": i['count'], "category": i['day'].strftime('%m/%d'),
                 "name": _("Total exchanges")}
            )
            day_bar_chart["graph_data"]["data"].append(
                {"value": i['sum'], "category": i['day'].strftime('%m/%d'),
                 "name": _("Total values")}
            )

        return day_bar_chart

    def get_instafollow_user_table(self):
        instafollow_users_table_headers = [
            _('Instagram Accounts'), _("Fast Charge User")
        ]
        instafollow_users_table = table_pattern(
            _("Instagram account details"),
            instafollow_users_table_headers
        )
        instafollow_users_table["graph_data"][
            'data'] = self.distinct_instafollow_users()

        return instafollow_users_table

    def get_fast_charge_users_table(self):
        fast_charge_users_table_headers = [
            _("Fast Charge User"), _('Instagram Accounts')
        ]
        fast_charge_users_table = table_pattern(
            _("Fast Charge users instagram accounts"),
            fast_charge_users_table_headers
        )
        fast_charge_users_table["graph_data"][
            'data'] = self.distinct_fastcharge_users()

        return fast_charge_users_table

    def generate_main_tables(self):
        table_headers = [
            _("sender"), _('through'), _("receiver"),
            _("value"), _("created_time")
        ]
        table = table_pattern(_("Fast Charge exchanges"), table_headers)
        table["graph_data"]['summary'] = self.get_summary()
        table["graph_data"]['data'] = self.generate_data()

        return table

    def generate_full_report(self):
        report = generate_report_pattern(
            _("Fast Charge exchanges"),
            slugify("Fast Charge exchanges")
        )
        # Generate total message
        # report['message'].update(self.get_message())
        # Generate month bar chart
        report['data'].append(self.generate_month_bar_chart())
        # Generate last 30 days bar chart
        report['data'].append(self.generate_day_bar_chart())
        # Generate distinct tables
        report['data'].append(self.get_instafollow_user_table())
        report['data'].append(self.get_fast_charge_users_table())
        # Generate log table
        report['data'].append(self.generate_main_tables())
        # Add filters
        report['filters'].append(self.date_filter)

        return report

    def get(self, request, *args, **kwargs):
        return Response(self.generate_full_report(), status=200)

    def post(self, request, *args, **kwargs):
        if not self.check_date_filter[1]:
            return Response(_("Start date is after the end date"), status=400)
        return Response(self.generate_full_report(), status=200)


fast_charge_exchange_value = FCExchangeValue.as_view()
