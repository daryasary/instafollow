# Report Generator =============================================================
def generate_report_pattern(title, slug):
    return {
        'title': title,
        'slug': slug,
        'filters': [],
        'data': [],
        'message': {}
    }


# Filter Generator =============================================================
def filter_pattern(slug, filter_type, values):
    return {
        "filter_slug": slug,
        "filter_type": filter_type,
        "filter_values": values
    }


# Charts Generators ============================================================
def table_pattern(title, headers, actions=[]):
    return {
        "graph_title": title,
        "graph_type": 'table',
        "graph_data": {
            'head': headers,
            'data': []

        },
        "actions": actions,
        "summary": []
    }


def text_pattern(title, text='', actions=[]):
    return {
        "graph_title": title,
        "graph_type": 'text',
        "graph_data": text,
        "actions": actions
    }


def gauge_pattern(title, total, used, order, unit, actions=[]):
    return {
        "graph_title": title,
        "graph_type": 'gauge',
        "graph_data": {
            "total": total,
            "used": used,
            "order": order,
            "unit": unit
        },
        "actions": actions
    }


def chart_pattern(title, graph_type, x_title=None, y_title=None, actions=[]):
    return {
        "graph_title": title,
        "graph_type": graph_type,
        "graph_data": {
            "x_title": x_title,
            "y_title": y_title,
            "definitions": {},
            "data": []
        },
        "actions": actions
    }


def pie_pattern(title, graph_type, name, actions=[]):
    return {
        "graph_title": title,
        "graph_type": graph_type,
        "graph_data": {
            "name": name,
            "data": []
        },
        "actions": actions
    }


# Form Generators ==============================================================
def form_pattern(title, graph_data, graph_type='form', actions=[]):
    return {
        "graph_title": title,
        "graph_type": graph_type,
        "graph_data": graph_data,
        "actions": actions
    }
