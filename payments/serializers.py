from rest_framework import serializers

from .models import Gateway, Payment


class GatewaySerializer(serializers.ModelSerializer):
    class Meta:
        model = Gateway
        fields = ['id', 'title']


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = ['id']

    def to_representation(self, instance):
        data = super(PaymentSerializer, self).to_representation(instance)
        data["active_gateways"] = GatewaySerializer(instance=Gateway.objects.filter(active=True), many=True,
                                                    read_only=True).data
        data["pay_url"] = instance.get_url()
        return data
