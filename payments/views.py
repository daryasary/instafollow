from urllib.parse import urlencode

from django.conf import settings
from django.http import HttpResponse, JsonResponse
from django.db import transaction
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.core.urlresolvers import reverse
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from .models import Payment, Gateway


class PayView(View):
    # dispatch and method decorator part for csrf exempt in class generic view
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(PayView, self).dispatch(request, *args, **kwargs)

    def get(self, request):
        """
        check id and gateway and redirect user to related paying method
        """
        # check and validate parameters
        if not ('invoice_number' in request.GET or 'gateway' in request.GET):
            return aware_app(False)

        try:
            gateway = Gateway.objects.get(id=request.GET['gateway'],
                                          is_enable=True)
        except Gateway.DoesNotExist:
            return aware_app(False)

        try:
            payment = Payment.objects.get(
                invoice_number=request.GET['invoice_number'], paid_status=False)
        except Payment.DoesNotExist:
            return aware_app(False, gateway.gw_type)

        # assign gateway to payment
        payment.gateway = gateway
        payment.save(update_fields=['gateway'])

        # if payment is a bank payment type
        if gateway.gw_type == Gateway.TYPE_BANK:
            # render bank page ... this page redirect immediately to bank page ... temporary disabled
            # return aware_app({'invoice_number': False})
            return render_bank_page(request, payment.invoice_number,
                                    gateway.url, gateway.merchant_id,
                                    payment.amount,
                                    ResNum1=settings.PAYMENT['project_name'])

        # if payment is a bank payment type
        if gateway.gw_type == Gateway.TYPE_PSP:
            return aware_app({'invoice_number': str(payment.invoice_number)},
                             gateway.gw_type)

    @transaction.atomic
    def post(self, request):
        """
        this method use for bank response posts
        """
        payment_status = False
        received_data = request.POST
        invoice_number = received_data.get("ResNum") or request.GET.get(
            'invoice_number')
        default_gw_type = Gateway.TYPE_BANK

        if 'gateway' in request.GET:
            try:
                default_gw_type = Gateway.objects.values_list('gw_type',
                                                              flat=True).get(
                    id=request.GET['gateway'], is_enable=True)
            except Exception as e:
                pass

        # in this situation we check minimum keywords for diffrent banks ... if
        # a new bank added its keyword for invoice id must add with or statement
        if not invoice_number:
            if received_data.get("purchase_token"):
                default_gw_type = Gateway.TYPE_PSP
                JWT = JSONWebTokenAuthentication()
                user = JWT.authenticate(request)[0]
                payment_status = Payment.find_purchase(user,
                                                       received_data)
            return aware_app({'payment_status': payment_status},
                             default_gw_type)

        # check and validate parameters
        try:
            payment = Payment.objects.select_related(
                'gateway').select_for_update().get(
                invoice_number=invoice_number)
        except Payment.DoesNotExist:
            pass
        except Exception as e:
            pass
        else:
            if not payment.paid_status:
                payment_status = payment.verify(received_data)
            # payment_status = payment.paid_status
            return aware_app({'payment_status': payment_status},
                             payment.gateway.gw_type)

        return aware_app({'payment_status': payment_status}, default_gw_type)


def aware_app(result_params, gw_type=Gateway.TYPE_BANK):
    """
    redirect to a url that aware app about state of payment

    :param status: True or False in failure or success
    :return: a http response that whis yaraapp shema that aware app
    """

    if gw_type == Gateway.TYPE_BANK:
        # TODO: fix landing page from Bank
        # render_context = {
        #     "res_url": "zaferoon://payres?status=%d&id=%d" % (int(status), int(object_id)),
        # }
        # response = render(request, 'payment/res.html', context=render_context)
        response = HttpResponse("", status=302)
        response['Location'] = "yara%s://home?%s" % (
            settings.PAYMENT['project_name'], urlencode(result_params))

    else:
        response = JsonResponse(result_params)
    return response


def get_gateway_by_id(gateway_id):
    try:
        return Gateway.objects.get(id=gateway_id, active=True)
    except:
        return None


def render_bank_page(request, invoice_id, request_url, merchant_id, amount,
                     **kwargs):
    """
    send parameters to a template ... template contain a form include thease parameters
    this form automatically submit to bank url
    """
    render_context = {
        "invoice_id": invoice_id,
        "request_url": request_url,
        "merchant_id": merchant_id,
        # "redirect_url": "%s/pay/" % settings.BASE_URL,
        "redirect_url": request.build_absolute_uri(reverse('payment-gateway')),
        "amount": amount * 10,
        "extra_data": kwargs,
    }
    return render(request, 'payments/pay.html', context=render_context)
