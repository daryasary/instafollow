# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BazaarToken',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('access_token', models.CharField(verbose_name='bazaar access token', max_length=150)),
                ('created_on', models.DateTimeField(auto_now=True, verbose_name='Creation On')),
                ('modified_on', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Modified On', editable=False)),
            ],
        ),
        migrations.AlterField(
            model_name='payment',
            name='paid_status',
            field=models.NullBooleanField(default=False, verbose_name='is paid status'),
        ),
    ]
