# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0002_auto_20170215_1726'),
    ]

    operations = [
        migrations.DeleteModel(
            name='BazaarToken',
        ),
        migrations.AlterField(
            model_name='gateway',
            name='gw_code',
            field=models.PositiveSmallIntegerField(verbose_name='gateway code', choices=[(1, 'Saman'), (2, 'Shaparak'), (3, 'Raad'), (4, 'Bazaar')]),
        ),
    ]
