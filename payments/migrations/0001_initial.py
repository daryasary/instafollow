# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Gateway',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('created_time', models.DateTimeField(verbose_name='Creation On', auto_now_add=True)),
                ('updated_time', models.DateTimeField(verbose_name='Modified On', auto_now=True)),
                ('title', models.CharField(verbose_name='gateway title', max_length=100)),
                ('merchant_id', models.CharField(verbose_name='merchant id', max_length=50, blank=True, null=True)),
                ('merchant_pass', models.CharField(verbose_name='merchant pass', max_length=50, blank=True, null=True)),
                ('url', models.CharField(verbose_name='request url', max_length=150, blank=True, null=True)),
                ('check_url', models.CharField(verbose_name='pay check url', max_length=150, blank=True, null=True)),
                ('gw_code', models.PositiveSmallIntegerField(verbose_name='gateway code', choices=[(1, 'Saman'), (2, 'Shaparak'), (3, 'Raad')])),
                ('gw_type', models.PositiveSmallIntegerField(verbose_name='gateway type', choices=[(1, 'BANK'), (2, 'PSP')])),
                ('is_enable', models.BooleanField(verbose_name='is enable', default=True)),
            ],
            options={
                'verbose_name': 'gateway',
                'db_table': 'payments_gateways',
                'verbose_name_plural': 'gateways',
            },
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('created_time', models.DateTimeField(db_index=True, auto_now_add=True, verbose_name='Creation On')),
                ('updated_time', models.DateTimeField(verbose_name='Modified On', auto_now=True)),
                ('invoice_number', models.UUIDField(verbose_name='invoice number', default=uuid.uuid4, unique=True)),
                ('amount', models.PositiveIntegerField(verbose_name='payment amount', editable=False)),
                ('reference_id', models.CharField(db_index=True, verbose_name='reference id', max_length=100, blank=True)),
                ('user_reference', models.CharField(verbose_name='customer reference', max_length=100, blank=True)),
                ('result_code', models.CharField(verbose_name='result code', max_length=100, blank=True)),
                ('paid_status', models.NullBooleanField(verbose_name='is paid status', editable=False, default=False)),
                ('log', models.TextField(verbose_name='payment log', blank=True)),
                ('gateway', models.ForeignKey(to='payments.Gateway', null=True, verbose_name='payment gateway', blank=True, related_name='payments')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True, verbose_name='User')),
            ],
            options={
                'verbose_name': 'payment',
                'db_table': 'payments',
                'verbose_name_plural': 'payments',
            },
        ),
    ]
