from django.conf.urls import url

from .views import PayView

urlpatterns = [
    url(r'^gateway/$', PayView.as_view(), name='payment-gateway'),
]
