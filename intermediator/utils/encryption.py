from hashlib import md5
from base64 import b64encode
from Crypto.Cipher import DES3


def encrypt(data, secret):
    key = md5(secret.encode('utf8')).digest()
    key += key[:8]
    BS = 8  # block size
    PAD = (BS - len(data) % BS) * chr(BS - len(data) % BS)
    data += PAD

    des = DES3.new(key, DES3.MODE_ECB)
    res = des.encrypt(data)

    return b64encode(res).decode()
