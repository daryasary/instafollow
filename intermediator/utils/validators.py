from django.core import validators
from django.core.exceptions import ValidationError

from django.utils.translation import ugettext_lazy as _


def validate_phone_number(value):
    phone_number = validators.RegexValidator(
        regex=r'^989[0-3,9]\d{8}$',
        message=_('Enter a valid mobile number.'),
        code='invalid'
    )
    phone_number(value)


def validate_email(value):
    email = validators.EmailValidator(
        message=_('Receiver should be email or phone_number'), code='invalid'
    )
    email(value)


def validate_fs_receiver(value):
    try:
        validate_phone_number(value)
    except ValidationError:
        validate_email(value)
