from django.db import models
from django.db.models import Sum
from django.utils import timezone
from datetime import date

from account.models import User


class Service(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class ExchangeValue(models.Model):
    sender = models.ForeignKey(
        User,
        verbose_name='User of instafollow',
        related_name='exchanges')
    through = models.ForeignKey(
        Service,
        default=1,
        verbose_name='Mediator service',
        related_name='exchanges'
    )
    receiver = models.CharField(
        verbose_name='User of third party app',
        max_length=128
    )
    value = models.IntegerField()
    log = models.TextField(blank=True)
    created_time = models.DateTimeField(auto_now_add=True)

    def username(self):
        return self.sender.username

    def __str__(self):
        return '{} > {} trough {}'.format(
            self.sender,
            self.receiver,
            self.through
        )

    @classmethod
    def check_daily(cls, receiver):
        # today = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
        today = date.today()
        res = cls.objects.filter(
            receiver=receiver, created_time__gte=today, log='<Response [204]>'
        ).aggregate(sum=Sum('value'))
        return res['sum'] if res['sum'] else 0
        # if res['sum'] and res['sum'] > 4000:
        #     return True
        # return False

    @classmethod
    def check_tries(cls, receiver):
        today = date.today()
        res = cls.objects.filter(
            receiver=receiver, created_time__gte=today, log='<Response [204]>'
        ).count()
        return res
