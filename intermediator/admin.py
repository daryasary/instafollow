from django.contrib import admin

from intermediator.models import Service, ExchangeValue


class ExchangeValueAdmin(admin.ModelAdmin):
    list_display = ['sender', 'username', 'through', 'receiver',
                    'value', 'log', 'created_time']
    search_fields = ('sender__user__username', 'receiver', 'sender__username')

    def has_add_permission(self, request):
        return False


admin.site.register(Service)
admin.site.register(ExchangeValue, ExchangeValueAdmin)
