from constance import config
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from intermediator.serializers import ExchangeValueSerializer
from utils import get_app_user


class ExchangeAPIView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = ExchangeValueSerializer

    @staticmethod
    def generate_response(balance):
        return {
            'balance': balance,
            'url': config.FJ_APK_URL
        }

    def get(self, request, *args, **kwargs):
        user = get_app_user(request)
        return Response(self.generate_response(user.exchangeable_balance))


exchange = ExchangeAPIView.as_view()
