# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0010_user_username'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExchangeValue',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('receiver', models.CharField(verbose_name='User of third party app', max_length=128)),
                ('value', models.IntegerField()),
                ('log', models.TextField(blank=True)),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('sender', models.ForeignKey(verbose_name='User of instafollow', related_name='exchanges', to='account.User')),
            ],
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=128)),
            ],
        ),
        migrations.AddField(
            model_name='exchangevalue',
            name='through',
            field=models.ForeignKey(verbose_name='Mediator service', related_name='exchanges', default=1, to='intermediator.Service'),
        ),
    ]
