import json

import requests
from constance import config
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from intermediator.models import ExchangeValue
from intermediator.utils.notify import notify_admin
from intermediator.utils.validators import validate_fs_receiver
from intermediator.utils import encrypt
from order.models import Transaction
from utils import get_app_user


class ExchangeValueSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExchangeValue
        fields = ('receiver', 'value')

    @staticmethod
    def validate_receiver(value):
        validate_fs_receiver(value)
        return value

    def validate_value(self, value):
        user = get_app_user(self.context['request'])
        if value > user.exchangeable_balance:
            raise serializers.ValidationError(
                _('Requested value is more than available value')
            )

        if value > config.MAX_COIN_PER_EXCHANGE:
            raise serializers.ValidationError(
                _("Sorry! maximum coin exchange quantity is {}".format(
                    config.MAX_COIN_PER_EXCHANGE))
            )

        if value < 1:
            raise serializers.ValidationError(
                _('0 or negative exchange is not allowed')
            )

        return value

    def validate(self, attrs):
        if ExchangeValue.check_tries(attrs['receiver']) > config.MAX_EXCHANGE_IN_ONE_DAY:
            raise serializers.ValidationError(
                _("Maximum retries exceeded today!")
            )

        if (ExchangeValue.check_daily(attrs['receiver']) + attrs['value']) > config.EXCHANGE_NOTIFY_LIMIT:
            notify_admin('Malicious action from "{}" in instafollow'.format(
                attrs['receiver'])
            )

        tmp = {'user': attrs['receiver'], 'point': attrs['value']}
        data = encrypt(json.dumps(tmp), settings.FAST_SHARJ['secret'])
        response = requests.post(settings.FAST_SHARJ['url'], data)

        response.raise_for_status()

        attrs.update({'log': response})
        return attrs

    def create(self, validated_data):
        user = get_app_user(self.context['request'])
        exchange = ExchangeValue.objects.create(
            sender=user,
            receiver=validated_data['receiver'],
            value=validated_data['value'],
            log=validated_data['log']
        )
        Transaction.negative(exchange.sender, exchange.value, exchange=exchange)
        return exchange

    def to_representation(self, instance):
        ret = super(ExchangeValueSerializer, self).to_representation(instance)
        ret['response'] = instance.log
        ret['balance'] = instance.sender.balance
        return ret
