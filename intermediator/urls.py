from django.conf.urls import url

from intermediator.views import exchange

urlpatterns = [
    url(r'^', exchange, name='exchange')
]
