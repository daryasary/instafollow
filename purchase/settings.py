from collections import OrderedDict

import os
import datetime
from purchase.custom_settings import *

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '*by#=tk=^x&#44@k##3f627pms51#edxlfql)lfa@ji)*_t7ct'

DEBUG = CUSTOM_DEBUG

ALLOWED_HOSTS = CUSTOM_ALLOWED_HOSTS

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'custom_message',
    'rest_framework',
    'account',
    'ads',
    'order',
    'financial',
    'gift',
    'intermediator',
    'payments',
    'constance',
    'constance.backends.database',
)

SITE_ID = 1

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'purchase.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'purchase.wsgi.application'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '%s:11211' % CACHE_HOST,
        'KEY_PREFIX': 'instafallow',
    }
}
# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = DATABASES_CONFIG

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/
LANGUAGE_CODE = 'fa'
LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

TIME_ZONE = 'Asia/Tehran'

USE_I18N = True

USE_L10N = True

USE_TZ = False

REST_FRAMEWORK = {
    'NON_FIELD_ERRORS_KEY': 'error',
    'DEFAULT_THROTTLE_RATES': {
        'anon': '60/hour',
        'user': '60/hour'
    }
}

# JWT Settings
JWT_AUTH = {
    'JWT_ENCODE_HANDLER':
        'rest_framework_jwt.utils.jwt_encode_handler',

    'JWT_DECODE_HANDLER':
        'rest_framework_jwt.utils.jwt_decode_handler',

    'JWT_PAYLOAD_HANDLER':
        'rest_framework_jwt.utils.jwt_payload_handler',

    'JWT_PAYLOAD_GET_USER_ID_HANDLER':
        'rest_framework_jwt.utils.jwt_get_user_id_from_payload_handler',

    'JWT_RESPONSE_PAYLOAD_HANDLER':
        'rest_framework_jwt.utils.jwt_response_payload_handler',

    'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=30*24*60*60),
    'JWT_AUDIENCE': None,
    'JWT_ISSUER': None,

    'JWT_ALLOW_REFRESH': False,
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=30),

    'JWT_AUTH_HEADER_PREFIX': 'JWT',
}

PAYMENT = {
    'project_name': 'instagram'
}

# BAZAAR settings:
BAZAAR_URLPATTERN = BAZAAR_CONFIG['URLPATTERN']
BAZAAR_CLIENT_ID = BAZAAR_CONFIG['CLIENT_ID']
BAZAAR_CLIENT_SECRET = BAZAAR_CONFIG['CLIENT_SECRET']
BAZAAR_REFRESH_URL = BAZAAR_CONFIG['REFRESH_URL']
BAZAAR_REFRESH_TOKEN = BAZAAR_CONFIG['REFRESH_TOKEN']

CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'

CONSTANCE_CONFIG = OrderedDict([
    ('MAX_COIN_PER_TRANSFORM', (2000, 'Allowed coin per try')),
    ('MAX_TRANSFORM_IN_ON_DAY', (2, 'Allowed time daily')),
    ('MAX_COIN_PER_EXCHANGE', (5000, 'Allowed coin per try')),
    ('MAX_EXCHANGE_IN_ONE_DAY', (1, 'Allowed time daily')),
    ('EXCHANGE_NOTIFY_LIMIT', (4000, 'Max exchange to notify admin')),
    ('REGISTRATION_GIFT', (40, 'Pay to user at register time')),
    ('RECOMMENDER_GIFT', (50, 'Pay to user when recommend another')),
    ('RECOMMENDED_GIFT', (30, 'Pay to user when recommended')),
    ('RECOMMEND_MESSAGE', ('', 'Message to show after recommendation')),
    ('VERSION', (1.0, 'Lowest allowed version')),
    ('URL', ('', 'Latest version APK url')),
    ('FORCE', (False, 'Force update?')),
    ('LOGOUT', (False, 'Force to logout users?')),
    ('HMAC_SECRET_KEY', (
        '012a54f51c49aa8c5c322416ab1410909add32c966bbaa0fe3dc58ac43fd7ede',
        'Instagram secret key')),
    ('IG_SIG_KEY_VERSION', (4, 'Instagram signature key')),
    ('USER_AGENT', (
        'Instagram 9.2.0 Android (18/4.3; 320dpi; 720x1280; Xiaomi; HM 1SW; armani; qcom; en_US',
        'Instagram client agent')),
    ('WELCOME_MESSAGE', ('', 'Message that user will see for the first time')),
    ('CUSTOM_MESSAGE_HEADER',
     ('', 'Custom message header that shows to user at openning app')),
    ('CUSTOM_MESSAGE_BODY',
     ('', 'Custom message body that shows to user at openning app')),
    ('CUSTOM_MESSAGE_LINK',
     ('', 'Custom link that show in custom message button')),
    ('AFTER_ORDER_MESSAGE',
     ('', 'Custom message that shows to user after submiting an order')),
    ('MONTH_FIRST', (700, 'Gift for the first place of month rate')),
    ('MONTH_SECOND', (600, 'Gift for the second place of month rate')),
    ('MONTH_THIRD', (500, 'Gift for the third place of month rate')),
    ('MONTH_FORTH', (350, 'Gift for the forth place of month rate')),
    ('MONTH_FIFTH', (200, 'Gift for the fifth place of month rate')),
    ('MONTH_SIXTH', (100, 'Gift for the sixth place of month rate')),
    ('WEEK_FIRST', (300, 'Gift for the first place of week rate')),
    ('WEEK_SECOND', (250, 'Gift for the second place of week rate')),
    ('WEEK_THIRD', (200, 'Gift for the third place of week rate')),
    ('WEEK_FORTH', (150, 'Gift for the forth place of week rate')),
    ('WEEK_FIFTH', (100, 'Gift for the fifth place of week rate')),
    ('WEEK_SIXTH', (50, 'Gift for the sixth place of week rate')),
    ('DAY_FIRST', (100, 'Gift for the first place of day rate')),
    ('DAY_SECOND', (80, 'Gift for the second place of day rate')),
    ('DAY_THIRD', (70, 'Gift for the third place of day rate')),
    ('DAY_FORTH', (60, 'Gift for the forth place of day rate')),
    ('DAY_FIFTH', (40, 'Gift for the fifth place of day rate')),
    ('DAY_SIXTH', (20, 'Gift for the sixth place of day rate')),
    ('ADS_CLICKED', (20, 'Coin for user if clicked on ads')),
    ('ADS_CLOSED', (10, 'Coin for user if closed on ads')),
    ('FJ_APK_URL', ('', 'Fast sharj .apk download url'))
])

CONSTANCE_CONFIG_FIELDSETS = {
    'Transform': ('MAX_COIN_PER_TRANSFORM', 'MAX_TRANSFORM_IN_ON_DAY'),
    'Exchange': ('MAX_COIN_PER_EXCHANGE', 'MAX_EXCHANGE_IN_ONE_DAY',
                 'EXCHANGE_NOTIFY_LIMIT'),
    'Recommend': ('REGISTRATION_GIFT', 'RECOMMENDER_GIFT', 'RECOMMENDED_GIFT',
                  'RECOMMEND_MESSAGE'),
    'Update': ('FORCE', 'URL', 'VERSION', 'LOGOUT'),
    'Instagram Client': ('HMAC_SECRET_KEY', 'IG_SIG_KEY_VERSION', 'USER_AGENT'),
    'Messages': ('WELCOME_MESSAGE', 'CUSTOM_MESSAGE_HEADER',
                 'CUSTOM_MESSAGE_BODY', 'CUSTOM_MESSAGE_LINK',
                 'AFTER_ORDER_MESSAGE'),
    'GIFT_AWARDS': ('MONTH_FIRST', 'MONTH_SECOND', 'MONTH_THIRD', 'MONTH_FORTH',
                    'MONTH_FIFTH', 'MONTH_SIXTH', 'WEEK_FIRST', 'WEEK_SECOND',
                    'WEEK_THIRD', 'WEEK_FORTH', 'WEEK_FIFTH', 'WEEK_SIXTH',
                    'DAY_FIRST', 'DAY_SECOND', 'DAY_THIRD', 'DAY_FORTH',
                    'DAY_FIFTH', 'DAY_SIXTH',),
    'ADS': ('ADS_CLICKED', 'ADS_CLOSED', 'FJ_APK_URL'),
}

STATIC_URL = '/static/'
STATIC_ROOT = 'static'

# Celery configurations
CELERY_BROKER_URL = 'amqp://{}:{}@{}'.format(
    CELERY_BROKER_USER,
    CELERY_BROKER_PASS,
    CELERY_BORKER_HOST
)


# Tapligh configurations
TAPLIGH = {
    'url': TAPLIGH_URL,
    'token': TAPLIGH_TOKEN,
    'packageName': TAPLIGH_PACKAGE_NAME,
    'sdkVersion': TAPLIGH_SDK_VERSION
}

# FAST SHARJ configurations
FAST_SHARJ = {
    'url': FAST_SHARJ_URL,
    'secret': FAST_SHARJ_SECRET
}

# REPORTING TOKEN
REPORTING_TOKEN = PRIVATE_REPORTING_TOKEN

# Orders activity padding
ACTIVITY_PADDING = 15
