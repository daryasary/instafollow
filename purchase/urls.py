from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.flatpages import views
from django.views.decorators.cache import cache_page

urlpatterns = [
    url(r'^pages/(?P<url>.*)$', cache_page(60 * 60)(views.flatpage), name='django.contrib.flatpages.views.flatpage'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^orders/', include('order.urls')),
    url(r'^financial/', include('financial.urls')),
    url(r'^account/', include('account.urls')),
    url(r'^extract/', include('extractor.urls')),
    url(r'^payment/', include('payments.urls')),
    url(r'^gift/', include('gift.urls')),
    url(r'^message/', include('custom_message.urls')),
    url(r'^exchange/', include('intermediator.urls')),
    url(r'^reports/', include('report.urls')),
    url(r'^ads/', include('ads.urls')),
]
