# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0010_user_username'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdsViewLog',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('token', models.CharField(unique=True, max_length=128)),
                ('action', models.IntegerField(choices=[(2, 'Clicked'), (1, 'Closed')])),
                ('value', models.IntegerField()),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(to='account.User', related_name='ads')),
            ],
        ),
    ]
