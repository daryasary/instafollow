from django.contrib import admin

from ads.models import AdsViewLog


class AdsViewLogAdmin(admin.ModelAdmin):
    list_display = ['user', 'token', 'action', 'value', 'created_at']

    def has_add_permission(self, request):
        return False

admin.site.register(AdsViewLog, AdsViewLogAdmin)
