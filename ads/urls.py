from django.conf.urls import url

from ads.views import submit_advertisement

urlpatterns = [
    url(r'submit/$', submit_advertisement, name='submit_ad')
]
