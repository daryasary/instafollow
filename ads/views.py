from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from ads.models import AdsViewLog
from ads.serializers import AdsSerializer
from utils import get_app_user


class AdsAPIView(CreateAPIView):
    queryset = AdsViewLog.objects.all()
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = AdsSerializer

    def perform_create(self, serializer):
        serializer.save(user=get_app_user(self.request))

submit_advertisement = AdsAPIView.as_view()
