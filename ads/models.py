from django.db import models
from django.utils.translation import ugettext_lazy as _

from account.models import User


class AdsViewLog(models.Model):
    CLOSED = 1
    CLICKED = 2
    ACTION_CHOICES = (
        (CLICKED, _('Clicked')),
        (CLOSED, _('Closed'))
    )
    user = models.ForeignKey(User, related_name='ads')
    token = models.CharField(max_length=128, unique=True)
    action = models.IntegerField(choices=ACTION_CHOICES)
    value = models.IntegerField()
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.token
