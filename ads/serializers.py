import json

import requests

from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from constance import config

from ads.models import AdsViewLog
from order.models import Transaction


class AdsSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdsViewLog
        fields = ('token', 'action')

    def validate(self, attrs):
        url = settings.TAPLIGH['url']
        data = {
            'token': settings.TAPLIGH['token'],
            'packageName': settings.TAPLIGH['packageName'],
            'sdkVersion': settings.TAPLIGH['sdkVersion'],
            'verifyToken': attrs['token']
        }
        response = requests.post(url, data)
        if response.status_code != 200:
            raise serializers.ValidationError(_("Tapligh API is not available"))

        res = json.loads(response.text)

        if 'responseCode' in res and res['responseCode'] != 2001:
            raise serializers.ValidationError(_("Token is not valid"))

        return attrs

    def create(self, validated_data):
        if validated_data['action'] == AdsViewLog.CLOSED:
            validated_data.update({'value': config.ADS_CLOSED})
        elif validated_data['action'] == AdsViewLog.CLICKED:
            validated_data.update({'value': config.ADS_CLICKED})
        instance = super(AdsSerializer, self).create(validated_data)
        Transaction.positive(instance.user, instance.value, advertisement=instance)
        return instance

    def to_representation(self, instance):
        ret = super(AdsSerializer, self).to_representation(instance)
        ret['balance'] = instance.user.balance
        return ret
