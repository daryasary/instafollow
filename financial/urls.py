from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from financial.views import packages, menus, PaymentViewSet, transactions

router = DefaultRouter()
router.register(r'payment', PaymentViewSet)

urlpatterns = [
    url(r'^packages/$', packages, name='fetch_orders'),
    url(r'^menu/(?P<action>[-\w]+)/$', menus, name='fetch_menus'),
    url(r'^transactions/$', transactions, name='transactions'),
    url(r'^', include(router.urls), name='payment')
]
