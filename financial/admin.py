from django.contrib import admin

from financial.models import Package, Purchase, Tariff, Menu


class PackageAdmin(admin.ModelAdmin):
    list_display = ('title', 'coin', 'price', 'active', 'created_at')


class PurchaseAdmin(admin.ModelAdmin):
    list_display = ('user', 'package', 'created_at')


class TariffAdmin(admin.ModelAdmin):
    list_display = ('action', 'cache', 'pay')


class MenuAdmin(admin.ModelAdmin):
    list_display = ('action', 'quantity', 'initial_price', 'general_price')


admin.site.register(Package, PackageAdmin)
admin.site.register(Purchase, PurchaseAdmin)
admin.site.register(Tariff, TariffAdmin)
admin.site.register(Menu, MenuAdmin)
