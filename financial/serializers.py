from django.db import transaction
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers

from financial.models import Package, Menu, Purchase
from order.models import Transaction
from payments.models import Gateway, Payment


class PaymentInlineSerializer(serializers.ModelSerializer):
    gateway = serializers.CharField(source='gateway.title', read_only=True)

    class Meta:
        model = Payment
        fields = ['created_time', 'user_reference', 'invoice_number', 'gateway',
                  'paid_status']


class PackageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Package
        fields = ('id', 'title', 'coin', 'price')


class PaymentSerializer(serializers.ModelSerializer):
    gateway = serializers.SerializerMethodField()
    package_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Payment
        fields = ['id', 'created_time', 'invoice_number', 'package_id',
                  'gateway']
        read_only_fields = ('invoice_number',)
        extra_kwargs = {'amount': {'read_only': False}}

    def validate(self, attrs):
        pid = attrs.pop('package_id')
        try:
            package = Package.objects.get(id=pid)
        except Package.DoesNotExist():
            raise serializers.ValidationError(
                _('Sorry selected package does not exists.')
            )

        if not package.is_available():
            raise serializers.ValidationError(
                _('Sorry selected package is not available.')
            )
        else:
            attrs.update({'amount': package.price})
            attrs.update({'package': package})

        return attrs

    def get_gateway(self, obj):
        req = self.context['request']
        return list(
            dict(
                title=gw['title'],
                type=gw['gw_type'],
                url=obj.get_url(gw, req)
            ) for gw in
            Gateway.objects.filter(is_enable=True).values('id', 'title',
                                                          'gw_type')
        )


class PurchaseSerializer(serializers.ModelSerializer):
    gateway = serializers.SerializerMethodField()
    payment = PaymentInlineSerializer(read_only=True)

    class Meta:
        model = Purchase
        fields = ('id', 'package', 'payment', 'gateway', 'created_at')
        read_only_fields = ('amount', 'is_consumed')

    def create(self, validated_data):
        package = validated_data.get('package')
        user = validated_data.get('user')
        with transaction.atomic():
            payment = Payment.objects.create(amount=package.price, user=user)
            purchase = Purchase.objects.create(
                user=user.user, package=package, payment=payment
            )
        return purchase

    def get_gateway(self, obj):
        if not obj.payment:
            return []
        req = self.context['request']
        return list(
            dict(
                title=gw['title'],
                type=gw['gw_code'],
                url=obj.payment.get_url(gw, req)
            ) for gw in
            Gateway.objects.filter(is_enable=True).values('id', 'title',
                                                          'gw_code')
        )


class MenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = Menu
        fields = ('id', 'action', 'quantity', 'initial_price', 'general_price')


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ('purchase', 'order', 'transform', 'balance', 'exchange',
                  'advertisement', 'created_at')
