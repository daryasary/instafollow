from django.db import models
from account.models import User
from payments.models import Payment


class LikeMenuManager(models.Manager):
    def get_queryset(self):
        return super(LikeMenuManager, self).get_queryset().filter(action='L')


class FollowMenuManager(models.Manager):
    def get_queryset(self):
        return super(FollowMenuManager, self).get_queryset().filter(
            action='F')


class Package(models.Model):
    title = models.CharField(max_length=150)
    coin = models.IntegerField()
    price = models.IntegerField()
    active = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Package'

    def __str__(self):
        return self.title

    def is_available(self):
        return self.active


class Purchase(models.Model):
    user = models.ForeignKey(User)
    package = models.ForeignKey(Package)
    payment = models.OneToOneField(Payment, null=True, editable=False,
                                   related_name='purchase')
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Purchase'

    def __str__(self):
        return '{}_{}'.format(self.user, self.package)

    @property
    def paid(self):
        return self.payment.paid_status


class Tariff(models.Model):
    type_choices = (('L', 'Like'), ('F', 'Follow'), ('C', 'Comment'))
    action = models.CharField(max_length=1, choices=type_choices, default='L',
                              unique=True)
    pay = models.IntegerField()
    cache = models.IntegerField()

    class Meta:
        verbose_name = 'Tariff'

    def __str__(self):
        return '{}:{}'.format(self.action, self.cache)

    @classmethod
    def get_cache(cls, action):
        try:
            tariff = cls.objects.get(action=action)
            return tariff.cache
        except cls.DoesNotExist:
            return 0

    @classmethod
    def get_pay(cls, action):
        try:
            tariff = cls.objects.get(action=action)
            return tariff.pay
        except cls.DoesNotExist:
            return 0


class Menu(models.Model):
    type_choices = (('L', 'Like'), ('F', 'Follow'))
    action = models.CharField(max_length=1, choices=type_choices, default='L')
    quantity = models.IntegerField()
    initial_price = models.IntegerField(null=True, blank=True)
    general_price = models.IntegerField()

    objects = models.Manager()
    follow = FollowMenuManager()
    like = LikeMenuManager()

    class Meta:
        ordering = ['quantity', 'general_price']

    def __str__(self):
        return '{}_{}=>{}'.format(self.quantity, self.action,
                                  self.general_price)
