# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Package',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('coin', models.IntegerField()),
                ('price', models.IntegerField()),
                ('active', models.BooleanField()),
            ],
            options={
                'verbose_name': 'Package',
            },
        ),
        migrations.CreateModel(
            name='Purchase',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('package', models.ForeignKey(to='financial.Package')),
                ('user', models.ForeignKey(to='account.User')),
            ],
            options={
                'verbose_name': 'Purchase',
            },
        ),
    ]
