# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('financial', '0004_package_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='package',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='purchase',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
