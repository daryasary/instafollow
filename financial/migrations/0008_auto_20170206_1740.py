# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('financial', '0007_menu'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menu',
            name='initial_price',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='menu',
            name='quantity',
            field=models.IntegerField(),
        ),
    ]
