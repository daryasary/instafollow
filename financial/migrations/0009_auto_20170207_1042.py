# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('financial', '0008_auto_20170206_1740'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tariff',
            old_name='cost',
            new_name='cache',
        ),
        migrations.AddField(
            model_name='tariff',
            name='pay',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
