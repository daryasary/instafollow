# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('financial', '0006_tariff'),
    ]

    operations = [
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('action', models.CharField(max_length=1, choices=[('L', 'Like'), ('F', 'Follow')], default='L')),
                ('quantity', models.IntegerField(null=True, blank=True)),
                ('initial_price', models.IntegerField()),
                ('general_price', models.IntegerField()),
            ],
            options={
                'ordering': ['quantity', 'general_price'],
            },
        ),
    ]
