# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('financial', '0011_auto_20170214_1355'),
    ]

    operations = [
        migrations.AlterField(
            model_name='purchase',
            name='payment',
            field=models.OneToOneField(related_name='purchase', editable=False, to='payments.Payment', null=True),
        ),
    ]
