# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('financial', '0010_purchase_payment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='purchase',
            name='payment',
            field=models.OneToOneField(editable=False, null=True, to='payments.Payment'),
        ),
    ]
