# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('financial', '0005_auto_20170125_0819'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tariff',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('action', models.CharField(choices=[('L', 'Like'), ('F', 'Follow'), ('C', 'Comment')], max_length=1, unique=True, default='L')),
                ('cost', models.IntegerField()),
            ],
            options={
                'verbose_name': 'Tariff',
            },
        ),
    ]
