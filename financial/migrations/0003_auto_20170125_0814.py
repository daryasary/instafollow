# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('financial', '0002_auto_20170125_0812'),
    ]

    operations = [
        migrations.RenameField(
            model_name='purchase',
            old_name='purchase',
            new_name='created_at',
        ),
    ]
