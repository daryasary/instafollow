# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0001_initial'),
        ('financial', '0009_auto_20170207_1042'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchase',
            name='payment',
            field=models.ForeignKey(default=1, to='payments.Payment'),
            preserve_default=False,
        ),
    ]
