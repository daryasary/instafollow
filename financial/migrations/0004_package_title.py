# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('financial', '0003_auto_20170125_0814'),
    ]

    operations = [
        migrations.AddField(
            model_name='package',
            name='title',
            field=models.CharField(max_length=150, default='defaul'),
            preserve_default=False,
        ),
    ]
