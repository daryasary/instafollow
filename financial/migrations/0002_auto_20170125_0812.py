# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('financial', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='package',
            name='created_at',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2017, 1, 25, 8, 12, 44, 272426, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='purchase',
            name='purchase',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2017, 1, 25, 8, 12, 53, 590801, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
