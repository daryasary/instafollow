from django.db.models.signals import post_save
from django.dispatch import receiver

from order.models import Transaction
from payments.models import Payment


@receiver(post_save, sender=Payment)
def payment_post_save(sender, instance, **kwargs):
    if instance.paid_status and not instance._b_paid_status:
        purchase = instance.purchase
        package = purchase.package
        Transaction.positive(user=purchase.user, cost=package.coin,
                             purchase=purchase)
