from rest_framework import generics
from rest_framework import mixins
from rest_framework import status
from rest_framework.generics import ListAPIView, get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from financial.models import Package, Purchase, Menu
from financial.serializers import PackageSerializer, MenuSerializer, \
    PurchaseSerializer, TransactionSerializer
from financial.serializers import PaymentSerializer
from order.models import Transaction
from payments.models import Payment
from utils import LargeResultsSetPagination


class PaymentViewSet(mixins.CreateModelMixin, GenericViewSet):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


#
#
# class PurchaseViewSet(mixins.CreateModelMixin, GenericViewSet):
#     queryset = Purchase.objects.select_related('package', 'payment').all()
#     serializer_class = PurchaseSerializer
#     authentication_classes = (JSONWebTokenAuthentication)
#     permission_classes = (IsAuthenticated,)
#
#     def perform_create(self, serializer):
#         # TODO: move serializer create here...
#         # print(serializer.validated_data)
#         serializer.save(user=self.request.user)


class LoadPackages(ListAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = PackageSerializer
    queryset = Package.objects.filter(active=True).all()

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return PurchaseSerializer
        return self.serializer_class

    def post(self, request):
        """
        When user purchase a package one new row should add
        to 2 table, first: Transaction table to execute bought
        balance. second: Purchase table to store users history
        """
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_403_FORBIDDEN)


class LoadMenus(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = MenuSerializer()

    def get_queryset(self, action=None):
        if action == 'like':
            return Menu.like.all()
        elif action == 'follow':
            return Menu.follow.all()
        return Menu.objects.all()

    def list(self, request, action=None):
        queryset = self.get_queryset(action)
        serializer = MenuSerializer(queryset, many=True)
        return Response(serializer.data)


class TransactionView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = TransactionSerializer

    # pagination_class = LargeResultsSetPagination

    def get_queryset(self):
        return Transaction.visible.filter(user=self.request.user.user).order_by(
            '-created_at')


transactions = TransactionView.as_view()
packages = LoadPackages.as_view()
menus = LoadMenus.as_view()
