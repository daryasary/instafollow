from django.apps import AppConfig


class FinancialConfig(AppConfig):
    name = 'financial'

    def ready(self):
        import financial.signals.handlers #noqa