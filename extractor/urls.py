from django.conf.urls import url

from extractor.views import get_posts_direct_links

urlpatterns = [
    url(r'^get_feed/$', get_posts_direct_links, name='parse_html')
]