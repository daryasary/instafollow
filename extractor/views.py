from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from order.constants import url_media
from utils import extract_json_from_html


class GetPostDirectLinks(APIView):
    @staticmethod
    def generate_response(data):
        nodes = data['entry_data']['ProfilePage'][0]['user']['media']['nodes']
        tmp = list()

        for node in nodes:
            tmp.append({
                'thumbnail': node['thumbnail_src'],
                'url': url_media.format(node['code']),
                'comments': node['comments']['count'],
                'likes': node['likes']['count'],
                        })
        return tmp

    def post(self, request):
        html = request.POST.get('html')
        posts_data = extract_json_from_html(html)
        data = self.generate_response(posts_data)
        return Response(data, status=status.HTTP_200_OK)

get_posts_direct_links = GetPostDirectLinks.as_view()