from django.conf.urls import url

from gift.views import register_code

urlpatterns = [
    url(r'^register_code/$', register_code, name='register_code'),
]