from django.db.utils import IntegrityError
from django.utils.translation import ugettext_lazy as _

from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from gift.models import PublicCode
from order.models import Transaction


class RegisterGift(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)

    def get_user(self):
        return self.request.user.user

    def generate_response(self, coin):
        tmp = {'message': _("You've got {} coins").format(coin),
               'balance': self.request.user.user.balance}
        return tmp

    def post(self, request, *args, **kwargs):
        user = self.get_user()
        code = request.data.get('code', None)
        if code:
            try:
                p_code = PublicCode.available.get(code=code)
                p_code.log.create(user=user)
            except PublicCode.DoesNotExist:
                return Response(
                    _('Code is wrong'), status=status.HTTP_404_NOT_FOUND
                )
            except IntegrityError:
                return Response(
                    _('Code is used before'),
                    status=status.HTTP_406_NOT_ACCEPTABLE
                )
            else:
                Transaction.positive(user, p_code.coin)
                res = self.generate_response(p_code.coin)
                return Response(res, status=status.HTTP_200_OK)
        return Response(
            _('Enter code please'), status=status.HTTP_400_BAD_REQUEST
        )


register_code = RegisterGift.as_view()
