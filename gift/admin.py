from django.contrib import admin

from gift.models import PublicCode, GiftLog


class PublicCodeAdmin(admin.ModelAdmin):
    list_display = ('code', 'min_coin', 'max_coin', 'expire_time', 'created_time')
    search_fields = ('code',)


# class PrivateCodeAdmin(admin.ModelAdmin):
#     list_display = ('code', 'coin', 'used', 'expire_time', 'created_time')
#     search_fields = ('code',)
#     list_filter = ('used',)


admin.site.register(PublicCode, PublicCodeAdmin)
# admin.site.register(PrivateCode, PrivateCodeAdmin)
# admin.site.register(GiftLog)
