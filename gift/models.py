import random
from django.contrib.contenttypes.fields import GenericForeignKey, \
    GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils import timezone

from account.models import User


class GitCodeManager(models.Manager):
    def get_queryset(self):
        return super(GitCodeManager, self).get_queryset().filter(expire_time__gte=timezone.now)


class PublicGitCodeManager(GitCodeManager):
    def get_queryset(self):
        return super(GitCodeManager, self).get_queryset().filter(used=True)


class GiftCode(models.Model):
    code = models.CharField(max_length=32, unique=True)
    expire_time = models.DateTimeField()

    created_time = models.DateTimeField(auto_now=True)
    modified_time = models.DateTimeField(auto_now_add=True)

    objects = models.Manager()
    available = GitCodeManager()

    class Meta:
        abstract = True

    def __str__(self):
        return self.code


class PrivateCode(GiftCode):
    coin = models.IntegerField()
    used = models.BooleanField(default=False, editable=False)
    log = GenericRelation('GiftLog', related_query_name='private')

    objects = models.Manager()
    available = PublicGitCodeManager()


class PublicCode(GiftCode):
    min_coin = models.IntegerField()
    max_coin = models.IntegerField()
    log = GenericRelation('GiftLog', related_query_name='public')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.id:
            self.coin = random.randint(self.min_coin, self.max_coin)


class GiftLog(models.Model):
    user = models.ForeignKey(User)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    created_time = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = (('user', 'object_id', 'content_type'),)
