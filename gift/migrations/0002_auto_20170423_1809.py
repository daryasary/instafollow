# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gift', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='giftlog',
            unique_together=set([('user', 'object_id', 'content_type')]),
        ),
    ]
