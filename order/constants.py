default_thumbnail_url = 'https://scontent-sjc2-1.cdninstagram.com/t51.2885-19/s320x320/15534963_374773999543196_8119169493042724864_a.jpg'
default_username = 'YARAMOBILE'

url_likes = 'https://www.instagram.com/web/likes/{}/like/'
url_follow = 'https://www.instagram.com/web/friendships/{}/follow/'

url_media_detail = 'https://www.instagram.com/p/{}/?__a=1'
url_user_detail = 'https://www.instagram.com/{}/?__a=1'

url_media_likes = 'https://i.instagram.com/api/v1/media/{}/likers/'  # format(media_id)
url_profile_follow = 'https://i.instagram.com/api/v1/friendships/{}/followers/?rank_token={}'  # format(user_id, rank_token)

url_json_detail = '{}?__a=1'
url_media = 'https://www.instagram.com/p/{}/'

Like = 'L'
Follow = 'F'
Comment = 'C'

actions_pattern = {Like: url_likes, Follow: url_follow}
controls_pattern = {Like: url_media_likes, Follow: url_profile_follow}

logger_message = '[{}] ({}) {}'
