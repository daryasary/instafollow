import csv
import json
from django.core.management.base import BaseCommand
from order.models import *


class Command(BaseCommand):
    balance = dict()

    @staticmethod
    def get_final_balance():
        with open('final_balance-3.txt', 'r') as check:
            all = json.loads(check.read())
        return all

    def handle(self, *args, **kwargs):
        final_balance = self.get_final_balance()
        with open('abuse-report.csv', 'w') as abuse:
            fieldnames = ['account_id', 'user_id', 'instagram_id', 'username', 'abuse', 'balance', 'exchange', 'SUM(values)']
            wr = csv.writer(abuse, quoting=csv.QUOTE_ALL)
            wr.writerow(fieldnames)
            for id in final_balance:
                user = User.objects.get(pk=id[0])
                row = [user.id, user.user.id, user.user.username, user.username, id[1], user.balance]
                transactions = user.transaction_set.filter(exchange__isnull=False)
                if transactions.exists():
                    rec = list()
                    value = 0
                    for transaction in transactions:
                        rec.append(transaction.exchange.receiver)
                        value += transaction.exchange.value
                    row.extend([', '.join(rec), value])

                wr.writerow(row)
