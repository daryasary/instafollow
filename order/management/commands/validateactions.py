from django.core.management.base import BaseCommand
from order.models import Activity, Order
# from utils import validate


class Command(BaseCommand):
    help = "Check if users undo their activities or not"

    @staticmethod
    def get_relative_activities(order):
        return Activity.get_related_to_order(order)

    @staticmethod
    def get_available_orders():
        return Order.load_available()

    def handle(self, *args, **kwargs):
        orders = self.get_available_orders()
        for order in orders:
            activities = self.get_relative_activities(order)
            for activity in activities:
                # validate(activity)
                pass
