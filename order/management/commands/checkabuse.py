import os
import json
import operator
from django.core.management.base import BaseCommand
from order.models import *


class Command(BaseCommand):
    balance = dict()

    def cluster(self, users_list):
        for user in users_list:
            if user in self.balance.keys():
                self.balance[user] += 1
            else:
                self.balance[user] = 1

    def sort_users(self):
        sorted_balance = sorted(self.balance.items(),
                                key=operator.itemgetter(1))
        sorted_balance.reverse()
        return sorted_balance

    def handle(self, *args, **kwargs):
        orders = Order.objects.filter(reach_count__lt=-20)
        print("### {} oder fetched".format(orders.count()))
        counter = 0
        for order in orders:
            users = order.activity_set.values_list(
                'user__id', flat=True).order_by('-do_date')[
                    :abs(order.reach_count)]
            self.cluster(users)
            counter += 1
            print("### Order {} got {} users, counter:{}".format(order.id, len(users), counter))
            print('############ ', list(self.balance.keys())[:3])

        print("### Cluster process finished")

        final_balance = self.sort_users()

        print("### Sorting process finished")

        with open('final_balance-2.txt', 'w') as output:
            output.write(json.dumps(final_balance))
            print("#$# {}".format(os.path.realpath(output.name)))

        print("### Storing process finished")
