from datetime import timedelta

from constance import config
from django.db.models import Count
from django.utils import timezone

from account.models import User
from order.models import Transaction, Activity

now = timezone.now()


def simplify(items):
    tmp = list()
    for item in items:
        item['username'] = item.pop('user__username')
        tmp.append(item)
    return tmp


def cluster_rate(rate):
    cluster = dict()
    if len(rate) > 20:
        cluster = {'FIRST': rate[0], 'SECOND': rate[1], 'THIRD': rate[2],
                   'FORTH': rate[3:9], 'FIFTH': rate[10:19], 'SIXTH': rate[20:]}
    return cluster


def pay_coins(cluster, prefix):
    for key, users in cluster.items():
        coin = getattr(config, prefix + key)
        if isinstance(users, dict):
            try:
                user = User.objects.get(pk=users['user'])
            except User.DoesNotExist:
                pass
            else:
                Transaction.positive(user, coin)
        else:
            for u in users:
                try:
                    user = User.objects.get(pk=u['user'])
                except User.DoesNotExist:
                    pass
                else:
                    Transaction.positive(user, coin)


def calculate_month_rate():
    last_month = now - timedelta(days=30)

    month_queryset = Activity.objects.select_related(
        'user'
    ).filter(
        do_date__gte=last_month, confirm=True
    ).values(
        'user', 'user__username'
    ).annotate(
        count=Count('user')
    ).order_by('-count')[:50]

    # month_queryset = Transaction.objects.filter(
    #     created_at__gte=last_month).values('user',
    #                                        'user__username').annotate(
    #     count=Count('user')).order_by('-count')[:50]

    return simplify(month_queryset)


def calculate_week_rate():
    last_week = now - timedelta(days=7)

    week_queryset = Activity.objects.select_related(
        'user'
    ).filter(
        do_date__gte=last_week, confirm=True
    ).values(
        'user', 'user__username'
    ).annotate(
        count=Count('user')
    ).order_by('-count')[:50]

    return simplify(week_queryset)


def calculate_day_rate():
    today = now.replace(hour=0)

    day_queryset = Activity.objects.select_related(
        'user'
    ).filter(
        do_date__gte=today, confirm=True
    ).values(
        'user', 'user__username'
    ).annotate(
        count=Count('user')
    ).order_by('-count')[:50]

    return simplify(day_queryset)


def calculate_period_rate(start, end, step, prefix):
    if prefix not in ['DAY_', 'WEEK_', 'MONTH_']:
        return 'Bad prefix'
    for i in range(start - 1, end):
        start_day = now.replace(hour=0, minute=0, second=0, day=i)
        end_day = now.replace(hour=0, minute=0, second=0, day=i + step)
        print('Start: {} || End: {}'.format(start_day, end_day))
        queryset = Transaction.objects.filter(created_at__gte=start_day,
                                              created_at__lte=end_day).values(
            'user', 'user__username').annotate(count=Count('user')).order_by(
            '-count')[:50]
        cluster = cluster_rate(simplify(queryset))
        pay_coins(cluster, prefix)


def calculate_rate():
    month_rate = calculate_month_rate()
    week_rate = calculate_week_rate()
    day_rate = calculate_day_rate()
    return {
        'month': month_rate,
        'week': week_rate,
        'day': day_rate
    }


def get_mode(id):
    if id % 2 == 0:
        return '-reach_count',
    return 'reach_count',


def fetch_user_actions_list(user):
    actions_list = list(
        Activity.objects.select_related(
            'order'
        ).filter(
            user=user
        ).values_list(
            'order',
            flat=True
        )
    )
    return actions_list
