from django.conf.urls import url
from django.views.decorators.cache import cache_page

from order.views import fetch_orders, target_list, submit_action, transform_coin, \
    user_orders, report_orders, v2_fetch_orders, submit_actions, public_rate

urlpatterns = [
    url(r'^fetch/(?P<type>[-\w]+)/$', fetch_orders, name='fetch_orders'),
    url(r'^v2/fetch/(?P<type>[-\w]+)/$', v2_fetch_orders, name='fetch_orders'),
    url(r'^targets/$', target_list, name='targets'),
    url(r'^fetch/$', fetch_orders, name='fetch_all_orders'),
    url(r'^v2/fetch/$', v2_fetch_orders, name='fetch_paginated_orders'),
    url(r'^report/$', report_orders, name='report_orders'),
    url(r'^report/(?P<pk>[0-9]+)/$', report_orders, name='report_order'),
    url(r'^submitted/$', user_orders, name='user_orders'),
    url(r'^transform/$', transform_coin, name='transform'),
    url(r'^public_rate/$', cache_page(60 * 60)(public_rate), name='public_rate'),
    url('^submit_actions/$', submit_actions, name='submit_actions_list'),
    url('^submit_action/(?P<pk>[0-9]+)/$', submit_action, name='submit_action'),
]
