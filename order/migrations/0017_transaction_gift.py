# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0016_auto_20170220_1023'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='gift',
            field=models.BooleanField(default=False),
        ),
    ]
