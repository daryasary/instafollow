# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0009_auto_20170129_1345'),
    ]

    operations = [
        migrations.AddField(
            model_name='target',
            name='thumbnail',
            field=models.URLField(default='https://ig-s-b-a.akamaihd.net/hphotos-ak-xfa1/t51.2885-19/s150x150/15535204_233186490439801_8962241809124163584_a.jpg'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='target',
            name='username',
            field=models.CharField(default='hosein', max_length=64),
            preserve_default=False,
        ),
    ]
