# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0003_auto_20170125_0805'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2017, 1, 25, 8, 9, 24, 908597, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='transaction',
            name='activity',
            field=models.ForeignKey(blank=True, to='order.Activity', null=True),
        ),
        migrations.AlterField(
            model_name='transaction',
            name='purchase',
            field=models.ForeignKey(blank=True, to='financial.Purchase', null=True),
        ),
    ]
