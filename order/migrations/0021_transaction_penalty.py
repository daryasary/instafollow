# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0020_auto_20170418_1628'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='penalty',
            field=models.BooleanField(default=False),
        ),
    ]
