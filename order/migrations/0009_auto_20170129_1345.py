# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0008_auto_20170128_1150'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='order',
            field=models.ForeignKey(blank=True, null=True, to='order.Order'),
        ),
        migrations.AlterField(
            model_name='target',
            name='target_type',
            field=models.CharField(max_length=2, default='Pr', choices=[('Ps', 'Post'), ('Pr', 'Profile')]),
        ),
    ]
