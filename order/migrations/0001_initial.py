# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('confirm', models.BooleanField()),
                ('do_date', models.DateTimeField(auto_now=True)),
                ('undo_date', models.DateTimeField(blank=True)),
            ],
            options={
                'verbose_name': 'Activity',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order_type', models.CharField(max_length=1, choices=[('L', 'Like'), ('F', 'Follow'), ('C', 'Comment')], default='L')),
                ('quantity', models.IntegerField()),
                ('owner', models.ForeignKey(to='account.User')),
            ],
            options={
                'verbose_name': 'Purchase',
            },
        ),
        migrations.CreateModel(
            name='Target',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField()),
            ],
            options={
                'verbose_name': 'Target',
            },
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('balance', models.IntegerField()),
                ('user', models.ForeignKey(to='account.User')),
            ],
            options={
                'verbose_name': 'Transaction',
            },
        ),
        migrations.AddField(
            model_name='order',
            name='target',
            field=models.ForeignKey(to='order.Target'),
        ),
        migrations.AddField(
            model_name='activity',
            name='order',
            field=models.ForeignKey(to='order.Order'),
        ),
        migrations.AddField(
            model_name='activity',
            name='user',
            field=models.ForeignKey(to='account.User'),
        ),
    ]
