# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0018_auto_20170406_1432'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='reach_count',
            field=models.IntegerField(verbose_name='reach', db_index=True, default=0),
        ),
    ]
