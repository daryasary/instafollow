# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0010_auto_20170204_1151'),
    ]

    operations = [
        migrations.AlterField(
            model_name='target',
            name='thumbnail',
            field=models.URLField(blank=True),
        ),
        migrations.AlterField(
            model_name='target',
            name='username',
            field=models.CharField(max_length=64, blank=True),
        ),
    ]
