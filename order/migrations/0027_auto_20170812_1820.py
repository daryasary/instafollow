# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0026_transaction_advertisement'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='last_check_in',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='order',
            name='modified_at',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2017, 8, 12, 18, 20, 39, 524488)),
            preserve_default=False,
        ),
    ]
