# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ads', '0001_initial'),
        ('order', '0025_transaction_exchange'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='advertisement',
            field=models.ForeignKey(null=True, blank=True, to='ads.AdsViewLog'),
        ),
    ]
