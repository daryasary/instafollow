# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0022_reportedorder'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reportedorder',
            name='accept',
            field=models.IntegerField(choices=[(0, 'Recently'), (1, 'Accept'), (2, 'Deny')], default=0),
        ),
    ]
