# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0011_auto_20170204_1318'),
    ]

    operations = [
        migrations.AddField(
            model_name='target',
            name='external_id',
            field=models.CharField(default='123456789', max_length=32),
            preserve_default=False,
        ),
    ]
