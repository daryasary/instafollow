# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('financial', '0001_initial'),
        ('order', '0002_auto_20170125_0621'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='activity',
            field=models.ForeignKey(to='order.Activity', null=True),
        ),
        migrations.AddField(
            model_name='transaction',
            name='created_at',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2017, 1, 25, 8, 5, 43, 717276, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='transaction',
            name='purchase',
            field=models.ForeignKey(to='financial.Purchase', null=True),
        ),
    ]
