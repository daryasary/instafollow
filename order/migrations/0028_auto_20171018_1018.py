# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0027_auto_20170812_1820'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='order',
            options={'verbose_name': 'Order', 'ordering': ('priority', '-quantity', 'created_at')},
        ),
        migrations.AddField(
            model_name='order',
            name='priority',
            field=models.PositiveIntegerField(default=500),
        ),
    ]
