# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0006_target_user'),
    ]

    operations = [
        migrations.AddField(
            model_name='target',
            name='target_type',
            field=models.CharField(choices=[('po', 'Post'), ('pr', 'Profile')], max_length=2, default='pr'),
        ),
    ]
