# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('intermediator', '0001_initial'),
        ('order', '0024_auto_20170426_1056'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='exchange',
            field=models.ForeignKey(blank=True, null=True, to='intermediator.ExchangeValue'),
        ),
    ]
