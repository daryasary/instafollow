# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0023_auto_20170426_1054'),
    ]

    operations = [
        migrations.RenameField(
            model_name='reportedorder',
            old_name='accept',
            new_name='status',
        ),
    ]
