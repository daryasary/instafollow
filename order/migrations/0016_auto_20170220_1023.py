# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0015_auto_20170219_1723'),
    ]

    operations = [
        migrations.RenameField(
            model_name='transaction',
            old_name='Transform',
            new_name='transform',
        ),
    ]
