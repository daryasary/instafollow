# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0008_remove_user_external_id'),
        ('order', '0014_auto_20170213_1301'),
    ]

    operations = [
        migrations.CreateModel(
            name='Transform',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('cost', models.IntegerField()),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('destination', models.ForeignKey(related_name='user_destination', to='account.User')),
                ('source', models.ForeignKey(related_name='user_source', to='account.User')),
            ],
            options={
                'ordering': ['-date'],
            },
        ),
        migrations.AddField(
            model_name='transaction',
            name='Transform',
            field=models.ForeignKey(blank=True, null=True, to='order.Transform'),
        ),
    ]
