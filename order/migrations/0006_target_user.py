# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20170125_1423'),
        ('order', '0005_auto_20170125_0819'),
    ]

    operations = [
        migrations.AddField(
            model_name='target',
            name='user',
            field=models.ForeignKey(default=1, to='account.User'),
            preserve_default=False,
        ),
    ]
