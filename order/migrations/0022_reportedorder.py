# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0021_transaction_penalty'),
    ]

    operations = [
        migrations.CreateModel(
            name='ReportedOrder',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('accept', models.BooleanField(default=False)),
                ('created_time', models.DateTimeField(auto_now=True)),
                ('modified_time', models.DateTimeField(auto_now_add=True)),
                ('order', models.OneToOneField(to='order.Order', related_name='reported')),
            ],
        ),
    ]
