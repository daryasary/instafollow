# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0012_target_external_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='ended',
            field=models.BooleanField(default=False),
        ),
    ]
