from django.utils.html import format_html

from order.lib.templates import TRANSACTION_BROKER_BANNER


def broker_banner(color='white', bkcolor='gray',
                  title='Transaction', value='True'):
    banner = TRANSACTION_BROKER_BANNER.format(color=color, bkcolor=bkcolor,
                                              title=title, value=value)
    return format_html(banner)
