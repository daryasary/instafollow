import json
from datetime import date

import requests
from constance import config

from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers

from financial.models import Menu
from order.constants import url_user_detail
from order.models import Order, Target, Activity, Transaction, Transform

from account.models import User as AppUser


class TargetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Target
        fields = ('url', 'inapp', 'action_url', 'target_type', 'thumbnail', 'username')
        readonly_fields = ('inapp',)


class OrderSerializer(serializers.ModelSerializer):
    target = TargetSerializer()

    class Meta:
        model = Order
        fields = ('id', 'owner', 'target', 'cache', 'order_type', 'quantity')


class OrderSubmissionSerializer(serializers.ModelSerializer):
    target = TargetSerializer()
    menu_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Order
        fields = ('id', 'owner', 'target', 'menu_id')

    def validate(self, attrs):
        constant = {'F': 'Pr', 'C': 'Ps', 'L': 'Ps'}
        menu = Menu.objects.get(id=attrs['menu_id'])
        if constant[menu.action] != attrs['target']['target_type']:
            raise serializers.ValidationError(
                _("Requested action is not responsible for requested target.")
            )

        user = attrs['owner']
        if menu.general_price > user.balance:
            raise serializers.ValidationError(
                _("Sorry you don't have enough money to do this action"),
                code='authorization')
        return attrs

    def create(self, validated_data):
        target_data = validated_data.pop('target')
        user = validated_data.get('owner')
        menu_id = validated_data.pop('menu_id')
        menu = Menu.objects.get(id=menu_id)

        target, result = Target.objects.get_or_create(user=user, **target_data)
        try:
            order = Order.objects.get(target=target)
        except Order.DoesNotExist:
            order = Order.objects.create(target=target, order_type=menu.action,
                                         quantity=menu.quantity, owner=user)
        else:
            if order.ended:
                order.ended = False
            order.quantity += menu.quantity
            order.reach_count += menu.quantity
            order.save()
        Transaction.negative(user=user, cost=menu.general_price, order=order)
        return order


class UserOrderSerializer(serializers.ModelSerializer):
    target = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = ('target', 'order_type', 'quantity', 'reach', 'ended')

    def get_target(self, obj):
        t = obj.target
        return t.thumbnail


class ActivitySerializer(serializers.ModelSerializer):
    order = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Activity
        fields = ('order',)


class TransformSerializer(serializers.ModelSerializer):
    destination = serializers.CharField(max_length=64)

    class Meta:
        model = Transform
        fields = ('source', 'cost', 'destination')

    def validate(self, attrs):
        url = url_user_detail.format(attrs['destination'])
        if attrs['cost'] > config.MAX_COIN_PER_TRANSFORM:
            raise serializers.ValidationError(
                _("Sorry! maximum coin transform quantity is {}".format(
                    config.MAX_COIN_PER_TRANSFORM)))

        if attrs['cost'] > attrs['source'].exchangeable_balance:
            raise serializers.ValidationError(
                _("Sorry! you don't have enough coins to do this action"))

        today = date.today()
        transform = Transform.objects.filter(date__gte=today,
                                             source=attrs['source'])
        if transform.count() >= config.MAX_TRANSFORM_IN_ON_DAY:
            raise serializers.ValidationError(
                _("Sorry! only {} transforms are available in a day".format(
                    config.MAX_TRANSFORM_IN_ON_DAY)))
        result = requests.get(url)
        if result.status_code == 200:
            user_id = json.loads(result.text)['user']['id']
            try:
                django_user = User.objects.get(username=user_id)
                attrs['destination'] = django_user.user
            except User.DoesNotExist:
                raise serializers.ValidationError(
                    _("Sorry destination user is not "
                      "registered in our application"))
        else:
            raise serializers.ValidationError(_("Given username is wrong"))

        return attrs

    def to_representation(self, instance):
        ret = super(TransformSerializer, self).to_representation(instance)
        ret['balance'] = AppUser.objects.get(id=int(ret['source'])).balance
        return ret
