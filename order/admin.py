from django.contrib import admin
from django.contrib.admin.views.main import ChangeList

from order.models import Target, Order, Activity, Transaction, Transform, \
    ReportedOrder


class PositiveBalanceFilter(admin.SimpleListFilter):
    title = 'balance'

    parameter_name = 'balance'

    def lookups(self, request, model_admin):
        return (
            ('negative', 'negatives'),
            ('positive', 'positives'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'positive':
            return queryset.filter(balance__gt=0)
        if self.value() == 'negative':
            return queryset.filter(balance__lt=0)


class TransactionTypeFilter(admin.SimpleListFilter):
    title = 'type'
    parameter_name = 'type'

    def lookups(self, request, model_admin):
        return (
            ('transform', 'transforms'),
            ('activity', 'activities'),
            ('purchase', 'purchases'),
            ('exchange', 'exchange'),
            ('order', 'orders'),
            ('gift', 'gifts'),
            ('penalty', 'penalty'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'transform':
            return queryset.exclude(transform__isnull=True)
        if self.value() == 'activity':
            return queryset.exclude(activity__isnull=True)
        if self.value() == 'purchase':
            return queryset.exclude(purchase__isnull=True)
        if self.value() == 'order':
            return queryset.exclude(order__isnull=True)
        if self.value() == 'exchange':
            return queryset.exclude(exchange__isnull=True)
        if self.value() == 'gift':
            return queryset.filter(gift=True)
        if self.value() == 'penalty':
            return queryset.filter(penalty=True)


class CustomChangeList(ChangeList):
    def get_queryset(self, request):
        queryset = super(CustomChangeList, self).get_queryset(request)
        return queryset[:10000]


class ActiveOrderFilter(admin.SimpleListFilter):
    title = 'active'

    parameter_name = 'reach_count'

    def lookups(self, request, model_admin):
        return (
            ('active', 'active'),
            ('inactive', 'inactive'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'active':
            return queryset.filter(reach_count__gte=0)
        if self.value() == 'inactive':
            return queryset.filter(reach_count__lt=0)


class TargetAdmin(admin.ModelAdmin):
    list_display = ('user', 'url', 'username', 'target_type', 'external_id')
    raw_id_fields = ('user',)


class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'owner', 'target', 'order_type', 'quantity',
        'ended', 'reach_count', 'priority', 'modified_at'
    )
    list_filter = ('order_type', 'ended', ActiveOrderFilter)
    search_fields = ['owner__user__username', 'target__url']
    raw_id_fields = ('owner', 'target',)
    actions = ['set_suspend']
    list_editable = ['priority',]

    def set_suspend(self, request, queryset):
        for q in queryset:
            q.last_check_in = None
            q.ended = True
            q.save()
    set_suspend.short_description = 'Suspend selected Orders'


class ActivityAdmin(admin.ModelAdmin):
    list_display = ('order', 'user', 'confirm', 'do_date')
    # list_filter = ('order', 'user', 'confirm')


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('user', 'balance', 'broker', 'created_at')
    list_select_related = (
        'transform',
        'activity',
        'purchase',
        'order',
        'exchange',
        'advertisement'
    )
    list_filter = (PositiveBalanceFilter, TransactionTypeFilter)
    search_fields = ['user__user__username', 'user__username']
    raw_id_fields = ('user',)
    readonly_fields = ('transform', 'activity', 'purchase', 'order',
                       'exchange', 'advertisement')

    def get_changelist(self, request, **kwargs):
        return CustomChangeList

    # def has_change_permission(self, request, obj=None):
    #     return False


class ReportedOrderAdmin(admin.ModelAdmin):
    list_display = ('user', 'url', 'show_status', 'status')
    search_fields = ('user', 'url')
    list_filter = ('status',)
    raw_id_fields = ('order', )
    list_editable = ('status',)


admin.site.register(Target, TargetAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Activity, ActivityAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(Transform)
admin.site.register(ReportedOrder, ReportedOrderAdmin)
