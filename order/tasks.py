from celery.schedules import crontab
from celery.task import periodic_task

from order.utils import calculate_month_rate, calculate_week_rate, cluster_rate, \
    pay_coins, calculate_day_rate


@periodic_task(run_every=crontab(hour=23, minute=45))
def daily_gifts():
    prefix = 'DAY_'
    cluster = cluster_rate(calculate_day_rate())
    pay_coins(cluster, prefix)


@periodic_task(run_every=crontab(hour=0, minute=0, day_of_week=5))
def weekly_gifts():
    prefix = 'WEEK_'
    cluster = cluster_rate(calculate_week_rate())
    pay_coins(cluster, prefix)


@periodic_task(run_every=crontab(hour=0, minute=0, day_of_month=1))
def monthly_gifts():
    prefix = 'MONTH_'
    cluster = cluster_rate(calculate_month_rate())
    pay_coins(cluster, prefix)
