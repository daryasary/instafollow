import requests
from constance import config
from django.db import IntegrityError
from django.db import transaction
from django.db.models import F
from django.http.response import Http404
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _
from rest_framework import generics
from rest_framework import mixins
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from financial.models import Tariff
from order.models import Order, Target, Activity, Transaction, ReportedOrder
from order.serializers import OrderSerializer, TargetSerializer, \
    OrderSubmissionSerializer, TransformSerializer, \
    UserOrderSerializer
from order.utils import calculate_rate, get_mode, fetch_user_actions_list
from utils import LargeResultsSetPagination, get_app_user


class FetchOrders(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = OrderSerializer

    def get_user(self):
        return self.request.user.user

    def get_queryset(self, type=None):
        types = {'like': "L", 'follow': "F", 'comment': "C"}
        actions = fetch_user_actions_list(self.get_user)
        orders = Order.active.filter(reach_count__gte=0).exclude(
            id__in=actions).exclude(owner=self.get_user)

        if type:
            orders = orders.filter(order_type=types[type])

        mode = get_mode(int(self.request.user.username[-1]))
        orders = orders.order_by(*mode)
        orders = orders[:100]

        return orders

    def list(self, request, type=None, *args, **kwargs):
        queryset = self.get_queryset(type)
        serializer = OrderSerializer(queryset, many=True)
        return Response(serializer.data)

    @staticmethod
    def generate_response(data, user):
        data['balance'] = user.balance
        data['message'] = config.AFTER_ORDER_MESSAGE
        return data

    def post(self, request, type=None):
        """
        When this control receive a request, mean that user submitted an order
        :param request:
        :return response status code:
        """
        data = request.data
        app_user = request.user.user
        data.update({'owner': app_user.id})
        serializer = OrderSubmissionSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            data = self.generate_response(serializer.data, app_user)
            return Response(data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


class FetchOrdersV2(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = OrderSerializer
    pagination_class = LargeResultsSetPagination

    def get_user(self):
        return self.request.user.user

    def get_queryset(self, type=None):
        types = {'like': "L", 'follow': "F", 'comment': "C"}
        actions = fetch_user_actions_list(self.get_user)
        orders = Order.active.filter(reach_count__gt=0).exclude(
            id__in=actions).exclude(owner=self.get_user)

        if type:
            orders = orders.filter(order_type=types[type])

        mode = get_mode(int(self.request.user.username[-1]))
        orders = orders.order_by(*mode)

        return orders

    def list(self, request, type=None, *args, **kwargs):
        queryset = self.get_queryset(type)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = OrderSerializer(queryset, many=True)
        return Response(serializer.data)

    @staticmethod
    def generate_response(data, user):
        data['balance'] = user.balance
        data['message'] = config.AFTER_ORDER_MESSAGE
        return data

    def post(self, request, type=None):
        """
        When this control receive a request, mean that user submitted an order
        :param request:
        :return response status code:
        """
        data = request.data
        app_user = request.user.user
        data.update({'owner': app_user.id})
        serializer = OrderSubmissionSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            data = self.generate_response(serializer.data, app_user)
            return Response(data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


class FetchUserOrders(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = UserOrderSerializer

    def get_user(self):
        return self.request.user.user

    def get_queryset(self, type=None):
        return Order.objects.filter(owner=self.get_user())


class ReportOrder(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)

    def get_object(self):
        order = get_object_or_404(Order, id=self.request.data['order_id'])
        return order

    def perform_action(self, instance):
        # If a user reported an order and report was True
        # before suspending order pay action coin to the reporter
        self.pay_coin(instance)
        instance.set_suspend()

    def pay_coin(self, instance):
        user = get_app_user(self.request)
        cost = Tariff.get_cache(instance.order_type)
        try:
            with transaction.atomic():
                activity = Activity.objects.create(
                    order=instance,
                    user=user,
                    confirm=True
                )
                Transaction.positive(user=user, cost=cost, activity=activity)
                instance.reach_count = F('reach_count') - 1
                instance.save()

        except IntegrityError:
            pass

    @staticmethod
    def check(instance):
        if instance.target.external_id == '':
            instance.target.save()
            return instance.target.external_id == ""
        result = requests.get(instance.target_url)
        return result.status_code == 404

    def post(self, request):
        instance = self.get_object()
        if self.check(instance):
            self.perform_action(instance)
            return Response(status=status.HTTP_202_ACCEPTED)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk):
        try:
            order = Order.objects.get(pk=pk)
        except Order.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            report, result = ReportedOrder.objects.get_or_create(order=order)
            if not result:
                report.status = ReportedOrder.NOT_SEEN
                report.save()
            return Response(status=status.HTTP_200_OK)


class TargetList(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)

    def get(self, request):
        user = request.user.user
        targets = Target.objects.filter(user=user).all()
        if targets:
            serializer = TargetSerializer(targets, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(
            {'data': _('No target found.')},
            status=status.HTTP_404_NOT_FOUND
        )


class SubmitAction(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)

    @staticmethod
    def load_order(pk):
        try:
            return Order.lives.get(pk=pk)
        except Order.DoesNotExist:
            raise Http404

    def put(self, request, pk):
        order = self.load_order(pk)
        user = request.user.user
        cost = Tariff.get_cache(order.order_type)

        try:
            with transaction.atomic():
                activity = Activity.objects.create(
                    order=order,
                    user=user,
                    confirm=True
                )
                Transaction.positive(user=user, cost=cost, activity=activity)
                order.reach_count = F('reach_count') - 1
                order.save()

        except IntegrityError:
            return Response(
                {
                    'data': 'Duplicate action for requested order',
                    'balance': user.balance
                },
                status=status.HTTP_403_FORBIDDEN
            )
        else:
            return Response(
                {
                    'data': _('Submitted successfully'),
                    'balance': user.balance
                },
                status=status.HTTP_201_CREATED)


class SubmitActionsList(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)

    def post(self, request):
        data = request.data
        orders = data.get('orders')
        cost = Tariff.get_cache(data.get('order_type'))
        user = request.user.user
        valid, invalid = 0, 0
        for i in orders:
            try:
                with transaction.atomic():
                    order = Order.lives.get(pk=i)
                    activity = Activity.objects.create(
                        order=order,
                        user=user,
                        confirm=True
                    )
                    Transaction.positive(user=user, cost=cost,
                                         activity=activity)
                    order.reach_count = F('reach_count') - 1
                    order.save()
                    valid += 1

            except Exception:
                invalid +=1
        return Response(
            {
                'data': _('valid:{valid}, invalid:{invalid}').format(valid=valid, invalid=invalid),
                'balance': user.balance
            },
            status=status.HTTP_201_CREATED)


class TransformCoin(mixins.CreateModelMixin, GenericViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = TransformSerializer

    def create(self, request, *args, **kwargs):
        data = request.data
        data.update({'source': request.user.user.id})
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data,
            status=status.HTTP_201_CREATED,
            headers=headers
        )


class PublicRate(APIView):
    # permission_classes = (IsAuthenticated,)
    # authentication_classes = (JSONWebTokenAuthentication,)

    def get(self, request):
        rate = calculate_rate()
        return Response(rate, status=status.HTTP_200_OK)


target_list = TargetList.as_view()
public_rate = PublicRate.as_view()
fetch_orders = FetchOrders.as_view()
report_orders = ReportOrder.as_view()
submit_action = SubmitAction.as_view()
user_orders = FetchUserOrders.as_view()
v2_fetch_orders = FetchOrdersV2.as_view()
submit_actions = SubmitActionsList.as_view()
transform_coin = TransformCoin.as_view({'post': 'create'})
