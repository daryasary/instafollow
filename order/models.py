from datetime import timedelta

import requests
from django.conf import settings

from django.utils import timezone
from django.db import models
from django.utils.html import format_html
from rest_framework.exceptions import ValidationError

from account.models import User
from ads.models import AdsViewLog
from financial.models import Purchase, Tariff
from intermediator.models import ExchangeValue
from order.constants import url_media_detail, url_user_detail, url_media_likes, \
    url_profile_follow, default_thumbnail_url, default_username, url_likes, \
    url_follow, url_json_detail
from order.lib import broker_banner


class LikeOrderManager(models.Manager):
    def get_queryset(self):
        return super(LikeOrderManager, self).get_queryset().filter(
            order_type='L')


class FollowOrderManager(models.Manager):
    def get_queryset(self):
        return super(FollowOrderManager, self).get_queryset().filter(
            order_type='F')


class CommentOrderManager(models.Manager):
    def get_queryset(self):
        return super(CommentOrderManager, self).get_queryset().filter(
            order_type='C')


class ActiveOrderManager(models.Manager):
    def get_queryset(self):
        return super(ActiveOrderManager, self).get_queryset().filter(
            ended=False)


class LiveOrderManager(models.Manager):
    def get_queryset(self):
        return super(LiveOrderManager, self).get_queryset().filter(
            reach_count__gte=-3, ended=False)


class Target(models.Model):
    type_choices = (('Ps', 'Post'), ('Pr', 'Profile'))
    user = models.ForeignKey(User)
    url = models.URLField()
    target_type = models.CharField(max_length=2, choices=type_choices,
                                   default='Pr')
    username = models.CharField(max_length=64, blank=True)
    thumbnail = models.URLField(blank=True)
    external_id = models.CharField(max_length=32)

    class Meta:
        verbose_name = 'Target'

    def __str__(self):
        return self.url

    @property
    def short_code(self):
        from urllib.parse import urlparse
        path = urlparse(self.url).path
        if self.target_type == 'Ps':
            return path.split('/')[2]
        elif self.target_type == 'Pr':
            return path.split('/')[1]
        else:
            return None

    @property
    def inapp(self):
        from urllib.parse import urlparse
        parse = urlparse(self.url)
        return 'http://' + parse.netloc[4:] + '/_u' + parse.path

    def fetch_preview_detail(self):
        context = dict()
        url = url_json_detail.format(self['url'])
        r = requests.get(url)
        if r.status_code == 200:
            data = r.json()
        if self['target_type'] == 'Pr':
            context['username'] = data['user']['username']
            context['thumbnail'] = data['user']['profile_pic_url']
        elif self['target_type'] == 'Ps':
            context['username'] = data['media']['owner']['username']
            context['thumbnail'] = data['media']['owner']['profile_pic_url']
        return context

    def save(self, *args, **kwargs):
        if self.target_type == 'Ps':
            url = url_media_detail.format(self.short_code)
            r = requests.get(url)
            if r.status_code == 200:
                data = r.json()
                if self.user.user.username != \
                        data['graphql']['shortcode_media']['owner']['id']:
                    raise ValidationError

                self.external_id = data['graphql']['shortcode_media']['id']
                self.username = data['graphql']['shortcode_media']['owner'][
                    'username']
                self.thumbnail = data['graphql']['shortcode_media'][
                    'display_url']
        elif self.target_type == 'Pr':
            url = url_user_detail.format(self.short_code)
            r = requests.get(url)
            if r.status_code == 200:
                data = r.json()
                if self.user.user.username != data['user']['id']:
                    raise ValidationError
                self.external_id = data['user']['id']
                self.username = data['user']['username']
                self.thumbnail = data['user']['profile_pic_url']
        else:
            return 'Instagram connection error'

        super(Target, self).save(*args, **kwargs)

    def action_url(self):
        if self.target_type == 'Ps':
            return self._get_like_url()
        elif self.target_type == 'Pr':
            return self._get_follow_url()

    action_url.short_description = 'Action'

    def _get_like_url(self):
        return url_likes.format(self.external_id)

    def _get_follow_url(self):
        return url_follow.format(self.external_id)

    def get_likers_or_followers_list(self):
        if self.target_type == 'Ps':
            url = url_media_likes.format(self.external_id)
        elif self.target_type == 'Pr':
            url = url_profile_follow.format(self.external_id, '123ranktoken')
        r = requests.get(url)
        if r.status_code == 200:
            data = r.json()['users']
            return [d["pk"] for d in data]
        else:
            return 'Bad request'


class Order(models.Model):
    type_choices = (('L', 'Like'), ('F', 'Follow'), ('C', 'Comment'))
    owner = models.ForeignKey(User)
    target = models.ForeignKey(Target)
    order_type = models.CharField(max_length=1,
                                  choices=type_choices,
                                  default='L')
    quantity = models.IntegerField()
    reach_count = models.IntegerField('reach', default=0, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    last_check_in = models.DateTimeField(null=True)
    ended = models.BooleanField(default=False)
    priority = models.PositiveIntegerField(default=500)

    objects = models.Manager()
    active = ActiveOrderManager()
    lives = LiveOrderManager()
    like = LikeOrderManager()
    follow = FollowOrderManager()
    comment = CommentOrderManager()

    class Meta:
        verbose_name = 'Order'
        ordering = ('priority', '-quantity', 'created_at')

    def __str__(self):
        return '{}_{}_{}_{}'.format(self.owner,
                                    self.target,
                                    self.order_type,
                                    self.quantity)

    def save(self, *args, **kwargs):
        if not self.id:
            self.reach_count = self.quantity
        super().save(*args, **kwargs)

    @property
    def available(self):
        if self.quantity < self.reach:
            self.ended = True
            return False
        return True

    @property
    def reach(self):
        return self.activity_set.filter(confirm=True).count()

    @property
    def target_url(self):
        return self.target.url

    @property
    def cache(self):
        return Tariff.get_cache(self.order_type)

    def set_suspend(self):
        self.ended = True
        self.save()

    def set_active(self):
        self.ended = False
        self.save()

    def incr(self, quantity=None):
        if quantity:
            self.reach_count += quantity
            # else:
            #     self.reach_count += 1
            self.save()

    def check_in(self):
        self.last_check_in = timezone.now()
        if self.ended:
            self.ended = False
        self.save()

    @classmethod
    def load_available(cls):
        # orders = cls.objects.all()
        # return [order for order in orders if order.available]
        return Order.objects.filter(reach_count__gte=0)


class Activity(models.Model):
    order = models.ForeignKey(Order)
    user = models.ForeignKey(User)
    confirm = models.BooleanField()
    do_date = models.DateTimeField(auto_now_add=True)
    undo_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        verbose_name = 'Activity'
        verbose_name_plural = 'Activities'
        unique_together = (("order", "user"),)

    def __str__(self):
        return '{}_{}_{}'.format(self.order, self.user, self.confirm)

    @property
    def target_external_id(self):
        return self.order.target.external_id

    @classmethod
    def get_related_to_order(cls, order):
        return cls.objects.filter(order=order, confirm=True).select_related(
            'order__target', 'user').all()

    def undo(self):
        self.confirm = False
        self.undo_date = timezone.now()
        self.save(update_fields=['confirm', 'undo_date'])


class Transform(models.Model):
    source = models.ForeignKey(User, related_name='user_source')
    destination = models.ForeignKey(User, related_name='user_destination')
    cost = models.IntegerField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} transformed {}$ to {}'.format(self.source, self.cost,
                                                 self.destination)

    class Meta:
        ordering = ['-date']

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        Transaction.negative(self.source, self.cost, transform=self)
        Transaction.positive(self.destination, self.cost, transform=self)


class TransactionManager(models.Manager):
    def get_queryset(self):
        return super(TransactionManager, self).get_queryset().filter(
            activity=None)


class Transaction(models.Model):
    user = models.ForeignKey(User)
    balance = models.IntegerField()
    transform = models.ForeignKey(Transform, null=True, blank=True)
    activity = models.ForeignKey(Activity, null=True, blank=True)
    purchase = models.ForeignKey(Purchase, null=True, blank=True)
    order = models.ForeignKey(Order, null=True, blank=True)
    exchange = models.ForeignKey(ExchangeValue, null=True, blank=True)
    advertisement = models.ForeignKey(AdsViewLog, null=True, blank=True)
    gift = models.BooleanField(default=False)
    penalty = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    objects = models.Manager()
    visible = TransactionManager()

    class Meta:
        verbose_name = 'Transaction'

    def __str__(self):
        return '{}:{}'.format(self.user, self.balance)

    def broker(self):
        attrs = {
            'activity': '#264653',
            'advertisement': '#2a9d8f',
            'purchase': '#0a2463',
            'order': '#f4a261',
            'transform': '#e76f51',
            'exchange': 'gray',
            'gift': 'green',
            'penalty': 'red'
        }
        for attr in attrs:
            value = getattr(self, attr)
            if value:
                return broker_banner(bkcolor=attrs[attr],
                                     title=attr, value=value)

    @classmethod
    def positive(cls, user, cost, activity=None, purchase=None, order=None,
                 transform=None, exchange=None, advertisement=None):
        if cost < 0:
            cost *= -1
        if activity:
            cls.objects.create(user=user, balance=cost, activity=activity)
        elif purchase:
            cls.objects.create(user=user, balance=cost, purchase=purchase)
        elif order:
            cls.objects.create(user=user, balance=cost, order=order)
        elif transform:
            cls.objects.create(user=user, balance=cost, transform=transform)
        elif exchange:
            cls.objects.create(user=user, balance=cost, exchange=exchange)
        elif advertisement:
            cls.objects.create(user=user, balance=cost,
                               advertisement=advertisement)
        else:
            cls.objects.create(user=user, balance=cost, gift=True)

    @classmethod
    def negative(cls, user, cost, activity=None, purchase=None, order=None,
                 transform=None, exchange=None):
        if cost > 0:
            cost *= -1
        if activity:
            cls.objects.create(user=user, balance=cost, activity=activity)
        elif purchase:
            cls.objects.create(user=user, balance=cost, purchase=purchase)
        elif order:
            cls.objects.create(user=user, balance=cost, order=order)
        elif transform:
            cls.objects.create(user=user, balance=cost, transform=transform)
        elif exchange:
            cls.objects.create(user=user, balance=cost, exchange=exchange)
        else:
            cls.objects.create(user=user, balance=cost, penalty=True)


class ReportedOrder(models.Model):
    NOT_SEEN = 0
    ACCEPT = 1
    DENY = 2
    STATUS_CHOICE = (
        (0, "Recently"),
        (1, "Accept"),
        (2, "Deny")
    )
    order = models.OneToOneField(Order, related_name='reported')
    status = models.IntegerField(choices=STATUS_CHOICE, default=0)

    created_time = models.DateTimeField(auto_now=True)
    modified_time = models.DateTimeField(auto_now_add=True)

    def url(self):
        return self.order.target_url

    def user(self):
        return self.order.owner

    def show_status(self):
        color = {0: 'Black', 2: 'red', 1: 'green'}
        for S in self.STATUS_CHOICE:
            if self.status == S[0]:
                if not self.status:
                    return format_html(
                        '<span style="color: {};"><b>{}</b></span>',
                        color[self.status], S[1])
                return format_html('<span style="color: {};">{}</span>',
                                   color[self.status], S[1])

    show_status.short_description = 'Status'

    def suspend_order(self):
        self.order.set_suspend()

    def active_order(self):
        self.order.set_active()
