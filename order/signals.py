from django.db.models.signals import post_save
from django.dispatch import receiver

from order.models import ReportedOrder


@receiver(post_save, sender=ReportedOrder)
def ReportAction(sender, instance, created, **kwargs):
    if instance.status == ReportedOrder.ACCEPT:
        instance.suspend_order()
    elif instance.status == ReportedOrder.DENY:
        instance.active_order()
