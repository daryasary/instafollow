from django.conf.urls import url

from .views import show_message

urlpatterns = [
    url(r'^$', show_message, name='show_message'),
]