from django.db import models


class Message(models.Model):
    body = models.TextField()

    created_time = models.DateTimeField(auto_now=True)
    modified_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.body
