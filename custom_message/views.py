from rest_framework.generics import RetrieveAPIView

from custom_message.models import Message
from custom_message.serializers import MessageSerializer


class ShowMessage(RetrieveAPIView):
    queryset = Message.objects.last()
    serializer_class = MessageSerializer

    def get_object(self):
        message = Message.objects.last()
        return message


show_message = ShowMessage.as_view()
