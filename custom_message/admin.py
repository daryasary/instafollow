from django.contrib import admin

from custom_message.models import Message


class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'body')


admin.site.register(Message, MessageAdmin)
