from django.conf.urls import url

from account.views import register, login, balance, recommend, force_update, \
    validate_user_activity

urlpatterns = [
    url(r'^register/$', register, name='register'),
    url(r'^login/$', login, name='login'),
    url('^balance/$', balance, name='user_balance'),
    url('^update/$', force_update, name='force_update'),
    url('^recommend/$', recommend, name='recommend'),
    url('^validate/$', validate_user_activity, name='validate_user_activity'),
]
