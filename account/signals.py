from constance import config
from django.db.models.signals import post_save
from django.dispatch import receiver

from order.models import Transaction
from .models import Recommendation


@receiver(post_save, sender=Recommendation)
def apply_transaction(sender, instance, created, **kwargs):
    if created:
        Transaction.positive(user=instance.recommender,
                             cost=config.RECOMMENDER_GIFT)
        Transaction.positive(user=instance.recommended,
                             cost=config.RECOMMENDED_GIFT)
