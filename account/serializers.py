import json
import http.client

from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers

from account.models import Recommendation


class UserSerializer(serializers.Serializer):
    cookie = serializers.CharField()

    def validate(self, attrs):
        cookie = attrs['cookie']
        conn = http.client.HTTPSConnection("www.instagram.com")

        headers = {
            'cookie': cookie
        }

        conn.request("GET", "/explore/", headers=headers)

        res = conn.getresponse()
        data = res.read()
        text = data.decode("utf-8")
        finder_text_start = ('<script type="text/javascript">'
                             'window._sharedData = ')
        finder_text_start_len = len(finder_text_start) - 1
        finder_text_end = ';</script>'

        all_data_start = text.find(finder_text_start)
        all_data_end = text.find(finder_text_end, all_data_start + 1)
        if all_data_end <= all_data_start:
            raise serializers.ValidationError(
                _('Sorry session id is not valid')
            )
        json_str = text[
                   (all_data_start + finder_text_start_len + 1): all_data_end]
        all_data = json.loads(json_str)
        viewer = all_data['config']['viewer']
        if viewer is None:
            raise serializers.ValidationError(
                _('Sorry submitted cookie is not valid')
            )

        else:
            attrs.update({'user_id': viewer['id']})
            attrs.update({'username': viewer['username']})
            attrs.update({'profile_pic_url': viewer['profile_pic_url']})

        return attrs


class RecommendationSerializer(serializers.ModelSerializer):
    code = serializers.CharField(max_length=32)

    class Meta:
        model = Recommendation
        fields = ('code',)

    def validate(self, attrs):
        try:
            username = attrs.pop('code')
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise serializers.ValidationError(
                _('Recommendation code is wrong!')
            )
        else:
            attrs.update({'recommender': user.user})

        return attrs

    def create(self, validated_data):
        try:
            recommendation = Recommendation.objects.create(**validated_data)
        except IntegrityError:
            raise serializers.ValidationError(
                _('Recommendation code is used'), code='authorization')
        return recommendation

    def to_representation(self, instance):
        data = {'balance': instance.recommended.balance}
        return data
