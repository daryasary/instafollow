# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0008_remove_user_external_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='Recommendation',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('created_time', models.DateTimeField(auto_now_add=True)),
                ('recommended', models.ForeignKey(to='account.User', related_name='recommended')),
                ('recommender', models.ForeignKey(to='account.User', related_name='recommender')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='recommendation',
            unique_together=set([('recommender', 'recommended')]),
        ),
    ]
