# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0004_user_cookie'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='instagram_username',
            field=models.CharField(blank=True, verbose_name='username', null=True, max_length=150),
        ),
    ]
