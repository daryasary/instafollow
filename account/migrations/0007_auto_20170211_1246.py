# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0006_auto_20170211_1227'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='instagram_user_id',
            new_name='external_id',
        ),
    ]
