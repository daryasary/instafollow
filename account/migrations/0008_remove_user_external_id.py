# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0007_auto_20170211_1246'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='external_id',
        ),
    ]
