# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0003_auto_20170131_0858'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='cookie',
            field=models.TextField(default='Empty Cookie For Test USERS'),
            preserve_default=False,
        ),
    ]
