# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20170125_1423'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='device_id',
            field=models.CharField(max_length=150, blank=True, null=True),
        ),
    ]
