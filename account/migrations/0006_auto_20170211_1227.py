# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0005_user_instagram_username'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='instagram_username',
            new_name='instagram_user_id',
        ),
    ]
