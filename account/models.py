from datetime import timedelta

from django.db import models
from django.contrib.auth.models import User as DjangoUser
from django.db.models import Sum
from django.utils import timezone
from django.conf import settings


class User(models.Model):
    user = models.OneToOneField(DjangoUser)
    device_id = models.CharField(max_length=150, null=True, blank=True)
    cookie = models.TextField()
    username = models.CharField(max_length=150, null=True, blank=True)

    class Meta:
        verbose_name = 'User'

    def __str__(self):
        return '{}'.format(self.user)

    @property
    def balance(self):
        temp = self.transaction_set.aggregate(balance=Sum('balance'))
        return temp['balance'] if temp['balance'] is not None else 0

    @property
    def blocked(self):
        period = timezone.now() - timedelta(days=settings.ACTIVITY_PADDING)
        temp = self.transaction_set.filter(
            activity__isnull=False,
            created_at__gte=period).aggregate(
            balance=Sum('balance'))
        return temp['balance'] if temp['balance'] is not None else 0

    @property
    def exchangeable_balance(self):
        exchangeable = self.balance - self.blocked - 40
        return exchangeable if exchangeable > 0 else 0


class Recommendation(models.Model):
    recommender = models.ForeignKey(User, related_name='recommender')
    recommended = models.ForeignKey(User, related_name='recommended')
    created_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (('recommender', 'recommended'),)
