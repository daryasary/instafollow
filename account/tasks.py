import json

from celery import shared_task
from datetime import timedelta

from celery.schedules import crontab
from celery.task import periodic_task
from django.utils import timezone
from django.conf import settings

from order.models import Order, Transaction, Activity


@shared_task()
def validate_orders(followers, user):
    period = timezone.now() - timedelta(days=settings.ACTIVITY_PADDING)
    orders = Order.objects.filter(owner=user, order_type='F')
    all_followers = list()
    for order in orders:
        order.check_in()
        activities = order.activity_set.select_related(
            'order', 'user'
        ).filter(
            confirm=True, do_date__gte=period
        )

        all_followers.extend(
            activities.select_related(
                'user'
            ).prefetch_related(
                'user__user'
            ).values_list(
                'user__user__username', flat=True
            )
        )

    unfollowed = set(all_followers).difference(followers)

    msg = {
        'owner': user,
        'orders': len(orders),
        'unfollowed': len(unfollowed)
    }

    # All penalties will extends user's last follow order only
    order = orders.last()
    if order:
        order.incr(len(unfollowed))

    for uid in unfollowed:
        activity = Activity.objects.select_related(
            'order', 'user'
        ).filter(
            user__user__username=uid,
            order__owner=user,
            confirm=True,
            do_date__gte=period
        ).first()
        # activity = activities.get(user__user__username=uid)
        Transaction.negative(user=activity.user, cost=2)
        activity.undo()

    return json.dumps(msg)


@periodic_task(run_every=crontab(hour=0, minute=0))
def disable_long_checked_orders():
    padding = timezone.now() - timedelta(days=settings.ACTIVITY_PADDING)
    orders = Order.lives.filter(last_check_in__lte=padding)
    orders.update(ended=True)
    return len(orders)
