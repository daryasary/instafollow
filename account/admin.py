from django.contrib import admin

from account.models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ('user', 'username', 'balance')
    search_fields = ['user__username', 'username']
    raw_id_fields = ('user',)


admin.site.register(User, UserAdmin)
