from datetime import timedelta

from constance import config

from django.db import transaction
from django.utils import timezone
from django.conf import settings

from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.serializers import JSONWebTokenSerializer, \
    jwt_payload_handler, jwt_encode_handler
from rest_framework_jwt.views import JSONWebTokenAPIView

from account.serializers import UserSerializer, RecommendationSerializer
from account.tasks import validate_orders
from order.models import Activity, Transaction
from utils import create_django_user, create_app_user, get_app_user


class SignUpUsers(APIView):
    @staticmethod
    def generate_token(user):
        payload = jwt_payload_handler(user)
        return jwt_encode_handler(payload)

    @staticmethod
    def get_or_create_django_user(user_id):
        user, result = create_django_user(user_id)
        return user, result

    @staticmethod
    def get_or_create_app_user(user, cookie, username):
        user = create_app_user(user, cookie, username)
        return user

    def generate_response(self, django_user, app_user, username, new,
                          profile_pic_url):
        return {'token': self.generate_token(django_user),
                'ds_user_id': django_user.username, 'username': username,
                'balance': app_user.balance, 'new': new,
                'profile_pic_url': profile_pic_url}

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user_id = serializer.validated_data['user_id']
            cookie = serializer.validated_data['cookie']
            username = serializer.validated_data['username']
            profile_pic_url = serializer.validated_data['profile_pic_url']
            django_user, result = self.get_or_create_django_user(user_id)
            app_user = self.get_or_create_app_user(user=django_user,
                                                   cookie=cookie,
                                                   username=username)
            response = self.generate_response(django_user,
                                              app_user,
                                              username,
                                              result,
                                              profile_pic_url)
            return Response(response, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


class LoginUser(JSONWebTokenAPIView):
    serializer_class = JSONWebTokenSerializer

    @staticmethod
    def generate_response(token, user):
        return {
            'token': token,
            'balance': user.balance
        }

    def post(self, request, *args, **kwargs):
        data = request.data
        # if 'token' in data:
        #     if data['token'] == 'EK4G6TlJLzNoUx8q':
        data['password'] = data['ds_user_id']
        data['username'] = data['ds_user_id']
        serializer = self.get_serializer(data=data)

        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            response_data = self.generate_response(token, user.user)

            return Response(response_data)

        return Response(status=status.HTTP_404_NOT_FOUND)


class UserBalance(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)

    @staticmethod
    def get_app_user(request):
        return request.user.user

    def get(self, request):
        user = self.get_app_user(request)
        data = {'balance': user.balance}
        return Response(data, status=status.HTTP_200_OK)


class Recommendation(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)

    def get(self, request):
        return Response({'code': request.user.username,
                         'message': config.RECOMMEND_MESSAGE})

    def post(self, request):
        serializer = RecommendationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(recommended=request.user.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_404_NOT_FOUND)


class ForceUpdateView(APIView):
    # permission_classes = (IsAuthenticated,)
    # authentication_classes = (JSONWebTokenAuthentication,)

    def get(self, request):
        response = {'force': config.FORCE,
                    'version': config.VERSION,
                    'url': config.URL,
                    'logout': config.LOGOUT,
                    'HMAC_SECRET_KEY': config.HMAC_SECRET_KEY,
                    'IG_SIG_KEY_VERSION': config.IG_SIG_KEY_VERSION,
                    'USER_AGENT': config.USER_AGENT,
                    'WELCOME_MESSAGE': config.WELCOME_MESSAGE,
                    'CUSTOM_MESSAGE':
                        {
                            'HEADER': config.CUSTOM_MESSAGE_HEADER,
                            'BODY': config.CUSTOM_MESSAGE_BODY,
                            'LINK': config.CUSTOM_MESSAGE_LINK
                        },
                    }

        return Response(response)


class ValidateUserActivities(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)

    def fetch_activities(self):
        period = timezone.now() - timedelta(settings.ACTIVITY_PADDING)
        return Activity.objects.filter(
            user=self.request.user.user,
            order__order_type='F',
            do_date__gte=period,
            confirm=True
        )

    def validate_activities(self, followings):
        activities = self.fetch_activities()
        targets = activities.values_list('order__target__external_id',
                                         flat=True)
        differences = list(set(targets).difference(followings))
        if len(differences):
            for target in differences:
                with transaction.atomic():
                    try:
                        activity = activities.select_related('order',
                                                             'user').get(
                            order__target__external_id=target)
                    except:
                        pass
                    else:
                        Transaction.negative(user=activity.user, cost=2)
                        activity.order.incr()
                        activity.undo()

    def get_user(self):
        return self.request.user.user.id

    def post(self, request):
        followers = request.data.get('followers', None)
        if followers:
            validate_orders.delay(followers, self.get_user())
        return Response('Done.', status=status.HTTP_200_OK)


login = LoginUser.as_view()
balance = UserBalance.as_view()
register = SignUpUsers.as_view()
recommend = Recommendation.as_view()
force_update = ForceUpdateView.as_view()
validate_user_activity = ValidateUserActivities.as_view()
