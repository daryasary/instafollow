import json

from constance import config
from django.contrib.auth.models import User as DjangoUser
from django.db import transaction
from django.db.utils import IntegrityError
from django.utils import timezone
from rest_framework.pagination import PageNumberPagination

from financial.models import Tariff
from order.constants import logger_message
from order.models import Transaction
from account.models import User as AppUser


def create_django_user(insta_id):
    try:
        user = DjangoUser.objects.create_user(username=insta_id,
                                              password=insta_id)
        return user, True
    except IntegrityError:
        user = DjangoUser.objects.filter(username=insta_id).first()
        return user, False
    except Exception as e:
        return None


def create_app_user(user, cookie, username, device_id=None):
    try:
        user, result = AppUser.objects.update_or_create(user=user, defaults={
            'cookie': cookie, 'device_id': device_id, 'username': username})
        if result:
            Transaction.positive(user=user, cost=config.REGISTRATION_GIFT)
        return user
    except Exception as e:
        return None


def get_app_user(request):
    return request.user.user


def get_user(request):
    return request.user


def extract_json_from_html(html):
    finder_text_start = ('<script type="text/javascript">'
                         'window._sharedData = ')
    finder_text_start_len = len(finder_text_start) - 1
    finder_text_end = ';</script>'

    all_data_start = html.find(finder_text_start)
    all_data_end = html.find(finder_text_end, all_data_start + 1)
    json_str = html[(all_data_start + finder_text_start_len + 1): all_data_end]
    all_data = json.loads(json_str)
    return all_data


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 30
    page_size_query_param = 'page_size'
    max_page_size = 100
